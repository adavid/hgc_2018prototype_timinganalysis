# HGC 2018 Prototype: Timing Performance Analysis

### Content
1. [Introduction](#Introduction)
2. [Experimental infrastructure](#Experimental-infrastructure)
3. [Data reconstruction](#Data-reconstruction)
4. [Scope of this study](#Scope-of-this-study)
5. [Requirements](#Requirements)
6. [Instructions](#Instructions-to-run-the-calibration-and-performance-assessment)
7. [Preliminary results](#Preliminary-results)
8. [Outlook](#Outlook)

## Introduction
The CMS High Granularity Calorimeter (HGCAL) is designed to provide timestamps of deposited energies with O(10ps) resolution.
In 2018, a prototype of this calorimeter was built and tested with high momentum particle beam at the H2 beam line at CERN. 
The used frontend ASIC is the SKIROC2-CMS chip which has prototype timing functionality consistent with the ultimate HGCROC design.

The accumulated test beam data provide a proof-of-concept of the envisioned HGCAL's timing capabilities. 
The code in this repository does the following:

* Per-channel determination of TOA-pedestals in approximately 24-hour periods.
* Per-channnel timing calibration.
* Per-channel timing performance evaluation using high-energetic positrons.
* Computation of a combined timestamp for a given particle shower and assessment of its resolution.

An integral part of this study is the active usage of a reference time obtained from two MCP's in the beam line. 
With it, the reliance on the clock-beam spill asynchronicity is less stringent than for the approaches in which the true particle arrival time w.r.t. clock edge is assumed to be uniformly distributed in a 25ns interval.

The first two paragraphs show the underlying experimental infrastructure as well as the reconstruction of the true raw data to ntuples which are used in this study. Links to further reading are indicated therein.
It follows a more precise definition of the scope of this study and the computing requirements for running this code.
Preliminary results and a mid-term outlook are provided at the end.

## Experimental infrastructure
In 2017/18, more than 90 HGCAL prototype modules were assembled and placed into sampling configurations for tests with particle beam at CERN's H2 beam line.

### Construction of HGC prototype modules
The HGCAL prototype modules consist of 6'', n-type silicon sensors with an active thickness of 300 or 200 microns. Each silicon sensor consists of 135 individual pads of different geometric shapes and sizes.

![](img/HGC_prototype_modules.png)

A particle going through the (depleted) silicon generates a signal which is readout by four [Skiroc2-CMS](https://iopscience.iop.org/article/10.1088/1748-0221/12/02/C02019/meta) ASICs.
This ASIC comprises prototype timing functionality via its TOA-rise and TOA-fall which correspond to the time-of-arrival of the induced signal within a clock cycle of 25ns (40MHz clock).

**For more information**, see [Chapter 4.5 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).


### Setup at the H2 beam line at CERN in October 2018
The prototype was made from two compartments, one being the electromagnetic one (CE-EE or EE) and the other being the hadronic one (CE-H or FH).
While the FH contained the majority of the available modules (9 layers with 7 modules each plus 3 layers with single modules), the EE was made of 28 layers with single modules. 
In good approximation, electromagnetic showers, e.g. due to positrons, were fully contained in the EE-compartment such that energy depositions in the FH stem from hadronically induced cascades.
Since the energy density for the latter is typically smaller than for the electromagnetic counterparts, a large statistics dataset of high energy densities- where the TOA measurement is expected to have optimal resolution- in the FH could not be achieved in the time-constrained test beam experiment.
Consequently, the focus of this study is on the electromagnetic section.

![](img/HGC_setup_H2_October2018.png)

The two compartments were placed in the H2 beam line. Electrons and charged pions with momenta ranging from 20-300 GeV/c were used as test particles.
Detector readout was initiated by the coincidence of two scintillators placed closely upstream the calorimeter.
In addition, impact position measurements were performed with delay wire chambers and - more importantly for this study - the particle-induced signals of two MCPs in front of the EE-compartment was recorded. 
The analysis of the recorded MCP waveforms allow the inference of the true particle incidence time within a clock cycle with resolutions of a few 10ps.
The picture below shows photographs of the associated readout hardware, of a delay wire chamber and the inside of one of the two MCPs.

![](img/Reference_Detectors.png)

**For detailed information**, see [Chapter 6 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).

## Data reconstruction

The data reconstruction's objective is to convert the raw bitstreams into calorimeter hits and MCP reference timestamps.
A calorimeter hit is defined to be an active calorimeter cell (synonyme: channel) with 3D spatial coordinates, a reconstructed energy (density) and the time-of-arrival of the deposited energy.

The reconstruction and calibration of the hit energy can be found elsewhere, e.g. in [Chapter 7.2 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).
Here, the reconstruction of the time-related quanitites is briefly summarised.

### TOA reconstruction
The SKIROC2-CMS ASIC provides two time-of-arrival (TOA) information for channels with high-enough signals, typically a few 10MIPs.
While the TOA-rise measures the time w.r.t. to the next rising clock edge, the TOA-fall measures it w.r.t to the next falling clock edge. In this sense, both information are highly correlated, as can be seen below. By design, they only differ by a constant of 25ns.

![](img/TOA_principle.png)

The conversion of the raw TOA as it is read out to actual calorimeter hit timestamps incorporates to aspects:

1. Calibration of the non-linearity of the TOA.
2. Calibration of the timewalk, i.e. the inherent dependence on the size of the induced signal.
Both aspects are illustrated in the figure above (Fig. 7.5)

![](img/TOA_formula1.png)

Formally, the TOA timestamp (T) is a function the digitised TOA ("rise" in the shown parameterisations above) and the calibrated hit energy E. 
For the TOA's non-linearity, it is beneficial to normalise the raw TOA by its minimum and maximum value to correct for potential pedestal variations.
The general form for the non-linearity and timewalk parameterisations are identical and are kept general. In particular, the non-linearity model could be simplified further to account for boundary conditions.

![](img/TOA_formula2.png)

**Note:** All parameters that occur in these formulas are subject to (channel-wise) calibration.

**For more information**, see [Chapter 7.2.5 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).

### MCP timestamp reconstruction
The MCP waveforms were digitised and recorded in the beam tests. Furthmore, the clock signal was injected into the digitiser and recorded.
Example waveforms of one readout are shown below. What may also be seen there is that the efficiency to detect those reproduces the MCP's acceptance.

![](img/MCP_timing.png)

In the waveform analysis, the MCP timestamp is defined to be always with respect to the last falling edge of the clock. This introduces a fixed offset w.r.t. the TOA's timescale.
The MCP's time is thus confined to the interval of 0 to 25ns.

![](img/MCP_formula.png)

**For more information**, see [Chapter 7.5 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).

## Scope of this study
The different timing information relevant to this study are sketched below.

![](img/time_measurement.png)

Provided that the calorimeter signal in a given channel is caused by the same particle that traverses the MCP, the reconstructed TOA timestamp (T) can be written as a sum of the MCP timestamp plus the time-of-flight (TOF) and the phase difference of the clock injected into the digitiser and into the ROC (CPD).

**It is assumed that TOF and CPD are channel-dependent but fixed constants,** or their variation is at least negligible.

![](img/TOA_MCP_relation1.png)

Then, the MCP timestamp can be written as a function of the measured hit energy and the TOA (scaled to [0, 1]).

![](img/TOA_MCP_relation2.png)

This relationship implies a unqiue conversion of the raw TOA and hit energy to the MCP time which serves as the absolute reference in this system.

---
### Research questions

1. Does the raw TOA in conjunction with the hit energy E exhibit the expected relationship to the reference time as measured by the MCP?
2. What is the timing accuracy and precision for each channel as a function of the hit energy?
3. What is the timing accuracy and precision when the timestamps of multiple hits in an electromagnetic shower are combined?

---

## Requirements
In order to run this analysis, please ensure that your computing system meets the following requirements.

### Hardware
* 21GB of free hard disk space for temporary files.
* 8GB RAM for loading combined datasets
* Access to the input data ntuples ("raw data") on ```/eos```: ```/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16``` 
 
### Software dependencies
* __**Python 2.7**__! 
* **luigi** for workflow management ([readthedocs](https://luigi.readthedocs.io/en/stable/))
* **pandas** for dataframe-based analysis ([installation](https://pandas.pydata.org))
* **root_pandas** for converting the ROOT-based ntuples into pandas datadframes([github](https://pandas.pydata.org))
* **scipy** for its statistics tools ([website](https://pandas.pydata.org))
* **tensorflow** (v1.12) for unbinned fitting of higher-dimensional models to data ([website](https://www.tensorflow.org/install))
* **matplotlib** for state-of-the-art data visualisation ([website](https://matplotlib.org))
* **numpy** for interfacing between pandas, matplotlib and scipy when necessary - also has some useful tools ([website](https://numpy.org))
* **tqdm** pretty printing of loop iterations similar to a progress bar ([github](https://github.com/tqdm/tqdm))



## Instructions to run the calibration and performance assessment
The analysis including timing calibration and performance assessment is implemented modularly in python scripts.
These scripts are steered by a luigi-based workflow. Each task therein, corresponds to a generic step of the analysis.
This repository defines command line commands to initiate the execution of such tasks.


### Setup
One has to define or modify a few environmental parameters and define the terminal commands that invoke the execution of the analysis steps.
For this purpose, the shell script provided in the main directory needs to be sourced: ```source setup.sh```.

### Luigi-based analysis workflow

Luigi is a data pipelining tool that helps in creating analysis workflow with intermediate files and in resolving dependencies between analysis steps in a natural way.
A nice example including an illustrative example is given in this [blog post](https://intoli.com/blog/luigi-jupyter-notebooks/).
Luigi has also been used for the steering of the HGCAL beam test data reconstruction, see [Chapter 7.1 of T.Q.'s doctoral thesis draft](http://tquast.web.cern.ch/tquast/thesis_draft.pdf).

The main idea is to implement the analysis in atomistic and generic steps. The steps themselves may be written in any programming language. Here it is Python.
Every step is then wrapped into a luigi task which can be toggled via the command line.

The figure below illustrates the full luigi-based workflow for this analysis starting from the data reduction all the way to the combined timing resolution of electromagnetic showers.
![](img/luigi_workflow.png)


### Description of implemented commands
The following commands are defined in ```setup.sh```and evoke a luigi task when executed in the terminal.

* ```ReduceNtuple <NTHREADS> <RUN>```

**(Data preprocessing)** Reads the ntuple content and saves timing information as flat pandas data frames for subsequent analysis independent of ROOT.

* ```TreatMinMax <NTHREADS> <RUNMIN> <RUNMAX>```
**(Data preprocessing)** Merges files of runs within the indicated run range (RUNMIN-RUNMAX) and counts the occurence of channels for identification of the minimum and maximum TOA value.
Normalises the TOA and writes out the affected channels into a file.
Only those channels for which the TOA range could be determined reliably can be used for subsequent calibration & timing performance assessment.

* ```DefineDatasets <NTHREADS>```
**(Dataset definition)** Determines the channels for which the number of entries with energies above a given threshold is sufficient for subsequent time calibration.
Splits the full dataset into a dataset used for calibration and one used for evaluation.
Writes out a list of those channels both as a .h5 file and (optional) as an ntuple for visualisation with [this event display](https://github.com/ThorbenQuast/HGCal_TB_Geant4) (requires [ROOT](https://root.cern.ch/)).

* ```CalibrateTimingForChannel <NTHREADS> <CHANNELKEY>```
**(Calibration)** Calibrates either the TOA-rise in a four-step procedure given the calibration dataset:

1. Correcting for the period of 25ns in the MCP-time vs. TOA relation.
2. (Pre-) calibration of the timewalk, i.e. dependence on the hit energy (=signal size).
3. (Pre-) calibration of the TOA nonlinearity after correcting the for the timewalk.
4. Unbinned 2D fit of the (TOA, hit energy) to the MCP timestamp.

* ```CalibrateTimingForAllChannels <NTHREADS>```
**(Calibration)** Wrapper for the ```CalibrateTimingForChannel``` task that executes it for all possible channels and also for the TOA-fall.

* ```ApplyTimingCalibration <NTHREADS>```
**(Reconstruction)** Applies the derived calibration constants to derive hit timestamps for TOA fall and rise independently.
The TOA fall is corrected by adding 25ns/2 = 12.5ns to match the TOA-rise based timestamp.
The period of the MCP time is similarly corrected.

* ```ComputeCommonTimestamp <NTHREADS>```
**(Reconstruction)** Computes the common timestamp (or rather the combined deviation) of the calibrated hit timestamps with respect to the MCP time for the evaluation dataset.
This is done for for the TOA-rise/fall and for both combined.

* ```EvaluateChannelTimingPerformance <NTHREADS> <CHANNELKEY>```
**(Evaluation)** Evaluates the timing performance of single channels in terms of accuracy and precision as a function of the hit energy using the calibration dataset. Produces the relevant distributions and graphs as graphic files.
Applied both for the TOA-rise/fall and both combined.

* ```EvaluateTimingForAllChannels <NTHREADS>```
**(Evaluation)** Wrapper around the ```EvaluateChannelTimingPerformance```task that executes it for all calibrated channels.

* ```EvaluateCombinedTimingPerformance <NTHREADS>```
**(Evaluation)** Evaluation strategy as for the channels but applied to the event timestamps from many channels combined. Applied both for the TOA-rise/fall and both combined. This could be the final figure of this study.


### Configuration files
The definitions of the input test beam ntuples and of the parameters associated to the tasks above are implemented in two configuration files:

* ```config/tb_ntuples.py```: Definition of the run list and location of the test beam ntuples that are used in this study.
* ```config/analysis.py```: Definition and explanation of analysis parameters used at various locations in this workflow.

## Preliminary results (status: 27 Feb 2020)
### MCP-only timing resolution
The reconstructed timestamp of MCP1 is compared to the one of MCP2.
Assuming that both MCP's have the same resolution, the difference divided by sqrt 2 yields an estimate of the MCP-only timing resolution. Hereby, it is important to select events for which the reconstructed waveform amplitude in both MCPs are similar.

![](img/MCP_resolution_formula.png)

To run this assessment, run ```python scripts/mcp/mcp_timing_resolution.py```.
The MCP-only time resolution converges towards a constant term of less than 10ps at high hit energies, whereas mean deviation between both MCPs (=the accuracy), fluctuates between 270-300 ps, i.e. by roughly 10%.
**```The reason for this fluctuation is not understood!```**

![](img/mcp_time_resolution.png)

**Note** that MCP2 is used only at this point, i.e. for estimating the timing resolution of MCP1.

### Calibration
Calibration can be performed both for the TOA-rise and the TOA-fall.
One example of corresponding figures for module 32, chip 2, channel 38 are shown below:

**For the TOA-rise:**
![](img/channel_32238_rise.png)

**For the TOA-fall:**
![](img/channel_32238_fall.png)

In total more than 300 channels in the electromagnetic section of the October 2018 HGCAL prototype could be calibrated.
The location of these channels (in yellow) is visualised in this graphic:

![](img/channel_location.png)

### Channel-wise performance
To assess the quality of the calibration, the accuracy and precision as a function of the hit energy is evaluated on the same dataset as used for calibration.
Events are selected for which the reconstructed MCP amplitude is at least 500 ADC counts (less than ~30ps MCP timing resolution).
For the example channel that was shown before:

![](img/channel_32238_eval.png)

The timing resolution (HGCAL+MCP) for the channels is around O(100ps) at high energy densities.

**Important comment**: The reported values here serve as rough estimates since the widths are derived from gaussian fits in the full range which are sensitive to outliers.

### Combined performance
Events are selected for which the reconstructed MCP amplitude is at least 500 ADC counts (less than ~30ps MCP timing resolution).
The timestamps of hits within an electromagnetic shower are combined through and energy-weigted average.
In this context, the MCP contribution of ~30ps ***has not*** been subtracted (in quadrature) yet.
![](img/performance_distributions.png)

The combined (i.e. HGCAL+MCP) resolution amounts to approximately 50ps at high shower energies. 
The accuracy exceeds 300ps at low shower energies and improves for energies above 50GeV. **```To be investigated!```**
Furthermore, there are clearly two populations in the calibrated TOA-rise to MCP vs. calibrated TOA-fall to MCP distribution. This could be due to the non-linearity at high TOA values which is not reached simultaneously by the TOA-fall and TOA-rise. **```To be investigated!```**

![](img/performance.png)

## Outlook
### Open questions

* Investigation of the two populations in the left-hand-side plot in the e+-timing performance figure.
* Cause for inaccuracies of the combined timestamp of around 300ps (absolute value). The per-channel inaccuracies are well below that value.
* Cause for the 10% variation in the mean time difference between MCP1 and MCP2?

### Possible enhancements

* Estimate the TOF using the delay wire chamber tracking. This could potentially minimise the contribution from TOF variations that otherwise would impair the measured timing resolution.
* Assess the impact of restricting the time reconstruction in each event and channel to the TOA which is in its linear range while excluding the other one.
* Tighten the constraints of the selection of channels when computing the combined shower timestamps.