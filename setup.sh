export WORKFLOWDIR=$PWD
export PYTHONPATH=$WORKFLOWDIR:$PYTHONPATH;



EvaluateCombinedTimingPerformance() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module evaluation Timing.EvaluateCombinedTimingPerformance --workers $1;
	cd $cwd;		
}

EvaluateTimingForAllChannels() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module evaluation Timing.EvaluateTimingForAllChannels --workers $1;
	cd $cwd;		
}

EvaluateChannelTimingPerformance() {
	cwd=$(pwd);
	CHKEY=$2
	cd $WORKFLOWDIR/tasks;
	luigi --module evaluation Timing.EvaluateChannelTimingPerformance --workers $1 --channelKey $CHKEY;
	cd $cwd;		
}

ComputeCommonTimestamp() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module reco Timing.ComputeCommonTimestamp --workers $1;
	cd $cwd;		
}

ApplyTimingCalibration() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module reco Timing.ApplyTimingCalibration --workers $1;
	cd $cwd;		
}

CalibrateTimingForAllChannels() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module calibration Timing.CalibrateTimingForAllChannels --workers $1;
	cd $cwd;		
}

CalibrateTimingForChannel() {
	cwd=$(pwd);
	CHKEY=$2
	cd $WORKFLOWDIR/tasks;
	luigi --module calibration Timing.CalibrateTimingForChannel --workers $1 --channelKey $CHKEY;
	cd $cwd;		
}

DefineDatasets() {
	cwd=$(pwd);
	cd $WORKFLOWDIR/tasks;
	luigi --module calibration Timing.DefineDatasets --workers $1;
	cd $cwd;		
}

TreatMinMax() {
	cwd=$(pwd);
	RUN_MIN=$2
	RUN_MAX=$3
	cd $WORKFLOWDIR/tasks;
	luigi --module preprocess Timing.TreatMinMax --workers $1 --runNumberMin $RUN_MIN --runNumberMax $RUN_MAX;
	cd $cwd;	
}

ReduceNtuple() {
	cwd=$(pwd);
	RUN=$2
	cd $WORKFLOWDIR/tasks;
	luigi --module preprocess Timing.FlattenNtuple --workers $1 --runNumber $RUN;
	cd $cwd;	
}