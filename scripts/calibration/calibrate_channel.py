#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Calibrates either the TOA rise or fall in four-step procedure:
#1. Correcting for the period of 25ns in the MCP-time vs. TOA relation
#2. (Pre-) calibration of the timewalk, i.e. dependence on the hit energy (=signal size)
#3. (Pre-) calibration of the TOA nonlinearity after correcting the for the timewalk
#4. Unbinned 2D fit of the (TOA, hit energy) to the MCP timestamp
#calibration parameters defined in config/analysis.py
from config.analysis import CALIBRATIONPARAMETERS
MINHITENERGY = CALIBRATIONPARAMETERS["MINHITENERGY"]
NBINS_TOA_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TOA_PRECALIB"]
NBINS_TW_PRECALIB = CALIBRATIONPARAMETERS["NBINS_TW_PRECALIB"]
MINENERGYFORTWCALIB = CALIBRATIONPARAMETERS["MINENERGYFORTWCALIB"]
MINENERGYFORTOACALIB = CALIBRATIONPARAMETERS["MINENERGYFORTOACALIB"]
NMAXFOR2DMINIMISATION = CALIBRATIONPARAMETERS["NMAXFOR2DMINIMISATION"]
QUANTILERANGESFORTW = CALIBRATIONPARAMETERS["QUANTILERANGESFORTW"]




import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
from scipy.stats import norm, gaussian_kde
from scipy.optimize import curve_fit
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import tensorflow as tf
from math import sqrt, isnan

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileCalib', type=str, help='Input file for calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration_preparation/samples_for_calibration.h5", required=True)
parser.add_argument("--channelKey", type=int, help="path to to ntuple to create channel map from", default=32136, required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibration", default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration/channel_32136.root", required=True)
parser.add_argument("--toaType", type=str, help="TOA type (fall or rise)", default="fall", required=True)
args = parser.parse_args()

calib_data = []
Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

toaType = args.toaType
TOA_TYPE = {"fall": "toa_fall", "rise": "toa_rise"}[toaType]



################################
#code snippet adapted from
#https://scikit-learn.org/stable/auto_examples/classification/plot_lda_qda.html#sphx-glr-auto-examples-classification-plot-lda-qda-py
def plot_data_after_discrimination(ax, lda1, lda2, X, y):
	ax.scatter(X[y==0][:,0], X[y==0][:,1],color="tab:blue")
	ax.scatter(X[y==1][:,0], X[y==1][:,1],color="tab:orange")
	ax.scatter(X[y==-1][:,0], X[y==-1][:,1],color="tab:brown")

	nx, ny = 200, 100
	x_min, x_max = ax.get_xlim()
	y_min, y_max = ax.get_ylim()
	xx, yy = np.meshgrid(np.linspace(x_min, x_max, nx),
	np.linspace(y_min, y_max, ny))
	Z = None
	if lda1 is not None:
		Z = lda1.predict_proba(np.c_[xx.ravel(), yy.ravel()])
		ax.plot(lda1.means_[0][0], lda1.means_[0][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
		ax.plot(lda1.means_[1][0], lda1.means_[1][1], '*', color='yellow', markersize=15, markeredgecolor='grey')

	if lda2 is not None:
		if Z is None:
			Z = -lda2.predict_proba(np.c_[xx.ravel(), yy.ravel()])
		else:
			Z -= lda2.predict_proba(np.c_[xx.ravel(), yy.ravel()])
		ax.plot(lda2.means_[0][0], lda2.means_[0][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
		ax.plot(lda2.means_[1][0], lda2.means_[1][1], '*', color='yellow', markersize=15, markeredgecolor='grey')
	
	Z = Z[:, 1].reshape(xx.shape)
	from matplotlib import colors
	ax.pcolormesh(xx, yy, Z, cmap='PRGn', norm=colors.Normalize(-1, 1.), zorder=0)



################################

#reading channels in files for calibration
store_key = "ch_%i"%args.channelKey
store = pd.HDFStore(args.inputFileCalib)
merged_data_calib = store[store_key]
store.close()

#step 0:
#select good timing data
selected_samples = merged_data_calib[(merged_data_calib.TCorrected>=0)&(merged_data_calib.TCorrected<=25.)&(merged_data_calib.hit_energy>MINHITENERGY)]
Nentries = len(selected_samples.hit_energy)

#prepare the figures
fig, axs = plt.subplots(3, 5, figsize=(30,15))


#try-except advantage: output figure is still produced
try: 
	#step 1) correct for different clock phases, depends on toa_rise or toa_fall
	if toaType=="rise":
		#a.) perform corner-based preclustering
		bottom_left_labels=np.array((selected_samples[TOA_TYPE]<0.2)&(selected_samples.TCorrected<5)).astype(int)
		top_right_labels=np.array((selected_samples[TOA_TYPE]>0.8)&(selected_samples.TCorrected>20)).astype(int)
		corner_labels = np.zeros(len(top_right_labels))+bottom_left_labels*(-1)+top_right_labels
		selected_samples[corner_labels==0].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:blue", ax=axs[0][0])
		if np.count_nonzero(corner_labels==1)>0:
			selected_samples[corner_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:orange", ax=axs[0][0])
		if np.count_nonzero(corner_labels==-1)>0:
			selected_samples[corner_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:brown", ax=axs[0][0])
		axs[0][0].set_title('1) Period correction', fontsize=20)
		axs[0][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[0][0].set_ylabel("Time from MCP [ns]")


		bottom_left_fisher_labels =  np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))
		top_right_fisher_labels = np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))

		clf_bl = None
		clf_tr = None

		if np.count_nonzero(bottom_left_labels==1)>0:
			clf_bl = LinearDiscriminantAnalysis()
			clf_bl.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(bottom_left_labels))
			bottom_left_fisher_labels = clf_bl.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		
		if np.count_nonzero(top_right_labels==1)>0:
			clf_tr = LinearDiscriminantAnalysis()
			clf_tr.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(top_right_labels))
			top_right_fisher_labels = clf_tr.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		corner_fisher_labels = np.zeros(len(top_right_fisher_labels))+bottom_left_fisher_labels*(-1)+top_right_fisher_labels
		
		plot_data_after_discrimination(axs[1][0], clf_bl, clf_tr, np.array(selected_samples[[TOA_TYPE, "TCorrected"]]), corner_labels)

		axs[1][0].set_title('Fisher Discriminant Analysis')
		axs[1][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[1][0].set_ylabel("Time from MCP [ns]")



		#c.) perform the shift
		selected_samples[corner_fisher_labels==0].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:blue", ax=axs[2][0])
		if np.count_nonzero(corner_fisher_labels==1)>0:
			selected_samples.loc[corner_fisher_labels==1, "TCorrected"] -= 25.		#top-right
			selected_samples[corner_fisher_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="darkmagenta", ax=axs[2][0])
		if np.count_nonzero(corner_fisher_labels==-1)>0:
			selected_samples.loc[corner_fisher_labels==-1, "TCorrected"] += 25.	#bottom-left
			selected_samples[corner_fisher_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="darkgreen", ax=axs[2][0])
		axs[2][0].set_title('After period correction')
		axs[2][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[2][0].set_ylabel("Period-corrected time from MCP [ns]")



	elif toaType=="fall":
		#a.) perform corner-based preclustering
		bottom_left_labels=np.array(25*(1.-selected_samples[TOA_TYPE])>selected_samples.TCorrected).astype(int)
		top_right_labels=np.array(25*(1.-selected_samples[TOA_TYPE])<selected_samples.TCorrected).astype(int)
		corner_labels = bottom_left_labels*(-1)+top_right_labels
		if np.count_nonzero(corner_labels==1)>0:
			selected_samples[corner_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:orange", ax=axs[0][0])
		if np.count_nonzero(corner_labels==-1)>0:
			selected_samples[corner_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="tab:brown", ax=axs[0][0])
		axs[0][0].set_title('1) Period correction', fontsize=20)
		axs[0][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[0][0].set_ylabel("Time from MCP [ns]")


		bottom_left_fisher_labels =  np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))
		top_right_fisher_labels = np.zeros(len(selected_samples[[TOA_TYPE, "TCorrected"]]))

		clf_bl = None
		clf_tr = None

		if np.count_nonzero(bottom_left_labels==1)>0:
			clf_bl = LinearDiscriminantAnalysis()
			clf_bl.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(bottom_left_labels))
			bottom_left_fisher_labels = clf_bl.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		
		if np.count_nonzero(top_right_labels==1)>0:
			clf_tr = LinearDiscriminantAnalysis()
			clf_tr.fit(selected_samples[[TOA_TYPE, "TCorrected"]], abs(top_right_labels))
			top_right_fisher_labels = clf_tr.predict(selected_samples[[TOA_TYPE, "TCorrected"]])

		corner_fisher_labels = +bottom_left_fisher_labels*(-1)+top_right_fisher_labels
		
		plot_data_after_discrimination(axs[1][0], clf_bl, clf_tr, np.array(selected_samples[[TOA_TYPE, "TCorrected"]]), corner_labels)

		axs[1][0].set_title('Fisher Discriminant Analysis')
		axs[1][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[1][0].set_ylabel("Time from MCP [ns]")


		#c.) perform the shift
		if np.count_nonzero(corner_fisher_labels==1)>0:
			selected_samples.loc[corner_fisher_labels==1, "TCorrected"] -= 12.5		#top-right
			selected_samples[corner_fisher_labels==1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="darkmagenta", ax=axs[2][0])
		if np.count_nonzero(corner_fisher_labels==-1)>0:
			selected_samples.loc[corner_fisher_labels==-1, "TCorrected"] += 12.5	#bottom-left
			selected_samples[corner_fisher_labels==-1].plot.scatter(y="TCorrected", x=TOA_TYPE, c="darkgreen", ax=axs[2][0])
		axs[2][0].set_title('After period correction')
		axs[2][0].set_xlabel("toa-"+toaType+" (normalised)")
		axs[2][0].set_ylabel("Period-corrected time from MCP [ns]")


	#step2: precalibrate the timewalk

	#a) plot the profiles for certain energy ranges
	selected_samples_forTW_precalib = selected_samples[(selected_samples.hit_energy>MINENERGYFORTWCALIB)]
	energy_quantile_ranges_forTW = sorted(np.array(selected_samples_forTW_precalib.hit_energy.quantile(QUANTILERANGESFORTW)))
	
	#binning in TOA-"+toaType+"
	bins = np.linspace(0., 1., NBINS_TW_PRECALIB)
	bin_centers = (bins [:-1] + bins [1:])/2

	energy_means = []
	tw_differences = []

	for quantile_index in range(0, len(energy_quantile_ranges_forTW)-1):
		color = ["black", "silver", "indianred", "red", "chocolate", "darkorange", "orange", "gold", "yellow", "lightgreen", "green", "aquamarine", "dodgerblue", "blue", "blueviolet", "magenta", "pink"][quantile_index]

		e_limit_min = energy_quantile_ranges_forTW[quantile_index]
		e_limit_max = energy_quantile_ranges_forTW[quantile_index+1]
		selected_samples_forTW_precalib_this_bin = selected_samples[selected_samples.hit_energy.between(e_limit_min, e_limit_max)]


		#compute the profile
		selected_samples_forTW_precalib_this_bin_grouped = selected_samples_forTW_precalib_this_bin.groupby(pd.cut(selected_samples_forTW_precalib_this_bin[TOA_TYPE], bins))
		plot_values = selected_samples_forTW_precalib_this_bin_grouped.TCorrected.mean()		
		axs[0][1].plot(bin_centers, plot_values, color, label="hit energy: %.1f-%.1f MIPs" % (e_limit_min, e_limit_max))

		energy_means.append(np.mean(selected_samples_forTW_precalib_this_bin.hit_energy))
		tw_differences.append(plot_values)

	axs[0][1].set_title('2) TW-precalibration', fontsize=20)
	axs[0][1].set_xlabel("x=toa-"+toaType+" [MIPs]")
	axs[0][1].set_ylabel("y=TRUTH (from MCP+TOF) [ns]")	
	axs[0][1].legend()


	#b) Compute the mean difference of the profiles as a function of the energy

	energy_means_forTWcalib = []
	tw_differences_for_TWcalib = []
	tw_differences_for_TWcalib_error = []
	n_energy_intervals = len(QUANTILERANGESFORTW)-1
	for bin_index in range(n_energy_intervals):
		difference_array = (tw_differences[bin_index]-tw_differences[int(n_energy_intervals/2)])[5:NBINS_TW_PRECALIB-5]
		difference_array = difference_array[False==(difference_array.isna())]
		if len(difference_array)==0:
			continue
		
		tw_difference_forTWcalib_error = np.std(difference_array)/sqrt(1.*len(difference_array))
		tw_difference_forTWcalib = np.mean(difference_array)

		energy_means_forTWcalib.append(energy_means[bin_index])
		tw_differences_for_TWcalib.append(tw_difference_forTWcalib)
		tw_differences_for_TWcalib_error.append(tw_difference_forTWcalib_error)

	energy_means_forTWcalib = np.array(energy_means_forTWcalib)
	tw_differences_for_TWcalib = np.array(tw_differences_for_TWcalib)
	tw_differences_for_TWcalib_error = np.array(tw_differences_for_TWcalib_error)
	def func_TW(x, a, b, c, d):
		return a * x + b + c/(x-d)
	
	axs[1][1].errorbar(energy_means_forTWcalib, tw_differences_for_TWcalib, yerr=tw_differences_for_TWcalib_error, label="Data points (N=%i)"%Nentries, color="forestgreen", marker="o", linestyle="None")
	
	popt_TW, pcov = curve_fit(func_TW, energy_means_forTWcalib, tw_differences_for_TWcalib, maxfev=1000000, p0=(0., 0.1, -400., -40.)) 
	x_points_for_plotting = np.linspace(min(selected_samples_forTW_precalib["hit_energy"]), max(selected_samples_forTW_precalib["hit_energy"]), 1000)
	axs[1][1].plot(x_points_for_plotting, func_TW(x_points_for_plotting, *popt_TW), 'firebrick', label='fit: a=%5.3f*x%+5.3f%+5.3f/(x-%5.3f)' % tuple(popt_TW))
	axs[1][1].set_title('TW-precalibration fit')
	axs[1][1].set_xlabel("x=mean hit energy [MIPs]")
	axs[1][1].set_ylabel("y=TRUTH - reference TRUTH at highest energy [ns]")	
	axs[1][1].set_ylim(-5., 5.)
	axs[1][1].legend()


	#c) apply the TW correction
	a_TW, b_TW, c_TW, d_TW = tuple(popt_TW)

	selected_samples_forTW_precalib = selected_samples_forTW_precalib.assign(TCorrected_TW = selected_samples_forTW_precalib.TCorrected - (a_TW*selected_samples_forTW_precalib.hit_energy + b_TW + c_TW/(selected_samples_forTW_precalib.hit_energy-d_TW)))

	axs[2][1].scatter(selected_samples_forTW_precalib[TOA_TYPE], selected_samples_forTW_precalib.TCorrected, label="Data (no TW correction) (N=%i)"%Nentries, color="darkblue")
	axs[2][1].scatter(selected_samples_forTW_precalib[TOA_TYPE], selected_samples_forTW_precalib.TCorrected_TW, label="Data (MCP time with TW correction) (N=%i)"%Nentries, color="magenta")
	axs[2][1].set_title("--> toa-"+toaType+" vs. MCP time")
	axs[2][1].set_xlabel("x=toa-"+toaType+" [MIPs]")
	axs[2][1].set_ylabel("y=TRUTH (from MCP+TOF) [ns]")
	axs[2][1].legend()


	#step 3: precalibrate the TOA function
	#a)
	selected_samples_forTOA_precalib = selected_samples_forTW_precalib[selected_samples_forTW_precalib.hit_energy>MINENERGYFORTOACALIB]

	bin_centers_TMCP, err_bin_centers, mean_toa, err_mean_toa = [], [], [], []
	err_bin_centers = []
	mean_toa = []
	err_mean_toa = []
	for interval in sorted(pd.qcut(selected_samples_forTOA_precalib["TCorrected_TW"], q=NBINS_TOA_PRECALIB).unique()):
		samples = selected_samples_forTOA_precalib[selected_samples_forTOA_precalib.TCorrected_TW.between(interval.left, interval.right)][TOA_TYPE]
		if len(samples)==0:
			continue
		bin_center = 0.5*(interval.left+interval.right)
		bin_centers_TMCP.append(bin_center)
		err_bin_centers.append((-interval.left+interval.right)/sqrt(12.))
		mean_toa.append(np.mean(samples))
		err_mean_toa.append(np.std(samples) / sqrt(2.*len(samples)))
	min_index = 2
	max_index = len(bin_centers_TMCP)-3
	if len(bin_centers_TMCP)<10:
		min_index=0
		max_index = len(bin_centers_TMCP)-1
	bin_centers_TMCP = np.array(bin_centers_TMCP[min_index:max_index])
	err_bin_centers = np.array(err_bin_centers[min_index:max_index])
	mean_toa = np.array(mean_toa[min_index:max_index])
	err_mean_toa = np.array(err_mean_toa[min_index:max_index])

	def func_TOA(x, a, b, c, d):
		return a * x + b + c/(x-d)


	popt_TOA, pcov = curve_fit(func_TOA, mean_toa, bin_centers_TMCP, maxfev=1000000, p0=(-15., 30., 4., 1.01))
	axs[0][2].scatter(mean_toa, bin_centers_TMCP, label="Data (MCP time with TW correction) (N=%i)"%Nentries, color="magenta", linestyle="None", marker="o")
	x_points_for_plotting = np.linspace(0.05, 0.95, 100)
	axs[0][2].plot(x_points_for_plotting, func_TOA(x_points_for_plotting, *popt_TOA), 'firebrick', label='fit: a=%5.3f*x%+5.3f%+5.3f/(x-%5.3f)' % tuple(popt_TOA))
	axs[0][2].set_title('3) TOA-precalibration', fontsize=20)
	axs[0][2].set_xlabel("x=TOA-"+toaType+" (normalised)")
	axs[0][2].set_ylabel("y=Period-corrected time from MCP [ns]")
	axs[0][2].legend()

	#b) apply the calibration
	a_TOA, b_TOA, c_TOA, d_TOA = tuple(popt_TOA)
	toa_calib_timestamp = a_TOA*selected_samples[TOA_TYPE] + b_TOA + c_TOA/(selected_samples[TOA_TYPE]-d_TOA) + (a_TW*selected_samples_forTW_precalib.hit_energy + b_TW + c_TW/(selected_samples_forTW_precalib.hit_energy-d_TW))
	#extend the data frame
	selected_samples = selected_samples.assign(toa_calib_timestamp =  toa_calib_timestamp)
	selected_samples = selected_samples.assign(delta_toa_calib_timestamp =  toa_calib_timestamp-selected_samples["TCorrected"])
	axs[1][2].hist2d(selected_samples["delta_toa_calib_timestamp"], selected_samples["hit_energy"], bins=50, range=[[-2.5, 2.5],[0., 1000.]], cmap=plt.cm.Reds, label="Data (N=%i)"%Nentries)
	axs[1][2].set_title('--> Timestamp difference vs. energy')
	axs[1][2].set_xlabel("RECO'-TRUTH [ns]")
	axs[1][2].set_ylabel("hit energy [MIPs]")
	axs[1][2].legend()

	#c) visualise the difference to the MCP timestamp
	frequencies, bins, patches = axs[2][2].hist(selected_samples.delta_toa_calib_timestamp, color="silver", bins=100, range=(-5., 5.), align="mid", density=True, label="Data (N=%i)"%Nentries)
	#TODO: need to fit gaussian to the weighted histogram!
	(mu, sigma) = norm.fit(selected_samples[abs(selected_samples.delta_toa_calib_timestamp)<5.].delta_toa_calib_timestamp)
	y_gaus = norm.pdf( bins, mu, sigma)
	axs[2][2].plot(bins, y_gaus, 'gold', linewidth=2, label=r"gaus-fit: $\mu$=%5.3fns, $\sigma$=%5.3fns" % (mu, sigma))
	axs[2][2].legend()
	axs[2][2].set_title('--> Timestamp difference')
	axs[2][2].set_xlabel("RECO'-TRUTH [ns]")
	axs[2][2].set_ylabel("Frequency (normalised)")



	#intermediate step: estimate the gaussian kernel density if the hit energy for inverse weights
	if Nentries > NMAXFOR2DMINIMISATION:
		print "Downsampling from",Nentries,"to",NMAXFOR2DMINIMISATION
		selected_samples = selected_samples.sample(frac=1.*NMAXFOR2DMINIMISATION/Nentries)
		Nentries = len(selected_samples.hit_energy)

	hit_energy_kernel = gaussian_kde(selected_samples.hit_energy)
	nbins = 100
	frequencies, bins, patches = axs[0][3].hist(selected_samples.hit_energy, bins=nbins, range=(0., max(selected_samples.hit_energy)), density="True", label="Data (N=%i)"%Nentries, color="orange")
	axs[0][3].plot(bins, hit_energy_kernel.evaluate(bins), "red", label="Gaussian KDE")
	axs[0][3].set_title("(Uniformising hit energy distribution)", fontsize=20)
	axs[0][3].set_xlabel("Hit energy [MIPs]")
	axs[0][3].set_ylabel("Frequency (normalised)")
	axs[0][3].legend()


	print "Computing energy weights"
	selected_samples = selected_samples.assign(energy_weights= 1./hit_energy_kernel.evaluate(selected_samples.hit_energy))	
	#selected_samples = selected_samples.assign(energy_weights= 1.)	
	frequencies, bins, patches = axs[1][3].hist(selected_samples.hit_energy, bins=nbins, weights=selected_samples.energy_weights,range=(0., max(selected_samples.hit_energy)), density="True", label="Data, weighted (N=%i)"%Nentries, color="chocolate")
	axs[1][3].set_title("Hit energy uniformisation, check")
	axs[1][3].set_xlabel("Hit energy [MIPs]")
	axs[1][3].set_ylabel("Frequency (normalised)")
	axs[1][3].legend()	


	#c) visualise the weighted difference to the MCP timestamp
	frequencies, bins, patches = axs[2][3].hist(selected_samples.delta_toa_calib_timestamp, weights=selected_samples.energy_weights, color="silver", bins=100, range=(-5., 5.), align="mid", density=True, label="Data, weighted")
	#TODO: need to fit gaussian to the weighted histogram!
	(mu, sigma) = norm.fit(selected_samples[abs(selected_samples.delta_toa_calib_timestamp)<5.].delta_toa_calib_timestamp)
	y_gaus = norm.pdf( bins, mu, sigma)
	axs[2][3].plot(bins, y_gaus, 'gold', linewidth=2, label=r"gaus-fit: $\mu$=%5.3fns, $\sigma$=%5.3fns" % (mu, sigma))
	axs[2][3].legend()
	axs[2][3].set_title('--> Timestamp difference')
	axs[2][3].set_xlabel("RECO'-TRUTH [ns]")
	axs[2][3].set_ylabel("Frequency (normalised)")


	#step 4: global, unbinned calibration with tensorflow

	#performing the minimisation with tensorflow
	with tf.device('/cpu:0'):
	    timing_input = tf.placeholder(tf.float32, [None, 2])
	    _a_TOA = tf.get_variable("a_TOA", shape=[1], initializer=tf.constant_initializer(value=a_TOA))
	    _c_TOA = tf.get_variable("c_TOA", shape=[1], initializer=tf.constant_initializer(value=c_TOA))
	    _d_TOA = tf.get_variable("d_TOA", shape=[1], initializer=tf.constant_initializer(value=d_TOA))
	    _a_TW = tf.get_variable("a_TW", shape=[1], initializer=tf.constant_initializer(value=a_TW))
	    _c_TW = tf.get_variable("c_TW", shape=[1], initializer=tf.constant_initializer(value=c_TW))
	    _d_TW = tf.get_variable("d_TW", shape=[1], initializer=tf.constant_initializer(value=d_TW))
	    _delta_l0 = tf.get_variable("delta_l0", shape=[1], initializer=tf.constant_initializer(value=+b_TOA+b_TW))

	    time_calib = _a_TOA * timing_input[:,0] + _c_TOA/(timing_input[:,0]-_d_TOA) + (_a_TW * timing_input[:,1] + _c_TW/(timing_input[:,1]-_d_TW)) + _delta_l0
	    time_true = tf.placeholder(tf.float32, [None])
	    delta_time_true2_inverse = tf.placeholder(tf.float32, [None])      #error on the MCP
	    learning_rate = tf.placeholder(tf.float32)
		


	##########
	minimized_function = tf.reduce_sum(tf.contrib.framework.sort(tf.abs(time_calib-time_true)*delta_time_true2_inverse))

	#compute MCP resolution
	#use parameterisation as in figure 7.15 of T. Quast's thesis
	selected_samples = selected_samples.assign(MCP_timing_error2 = 1./(15200**2/selected_samples["ampMCP1"]**2+9.3**2) )
	selected_samples = selected_samples.assign(weights_for_tf_min = selected_samples.energy_weights )

	########## Initializing the training ##########
	t_vars = tf.trainable_variables()
	t_vars1 = [var for var in tf.trainable_variables() if '_TOA' in var.name]
	t_vars2 = [var for var in tf.trainable_variables() if 'delta_l0' in var.name]
	train_step_1 = tf.train.AdamOptimizer(learning_rate).minimize(minimized_function, var_list=t_vars1)
	train_step_2 = tf.train.AdamOptimizer(learning_rate).minimize(minimized_function, var_list=t_vars2)

	config=tf.ConfigProto(log_device_placement=True)
	sess = tf.Session()
	init = tf.initialize_all_variables()
	sess.run(init)

	LearningRate = 0.1
	error_cost = []
	opt_parameters = []
	niterations = 0
	

	while True:	
		if niterations>0:
			sess.run([train_step_1, train_step_2], feed_dict={timing_input: selected_samples[[TOA_TYPE, "hit_energy"]], 
				time_true: selected_samples["TCorrected"], 
				delta_time_true2_inverse: selected_samples["weights_for_tf_min"], 
				learning_rate : LearningRate
			})

		error = sess.run(minimized_function, feed_dict={timing_input: selected_samples[[TOA_TYPE, "hit_energy"]], 
			time_true: selected_samples["TCorrected"],
			delta_time_true2_inverse: selected_samples["weights_for_tf_min"]
		})
		error_cost.append(error)
		this_opt_a_TOA = sess.run(_a_TOA)[0]
		this_opt_c_TOA = sess.run(_c_TOA)[0]
		this_opt_d_TOA = sess.run(_d_TOA)[0]
		this_opt_a_TW = sess.run(_a_TW)[0]
		this_opt_c_TW = sess.run(_c_TW)[0]
		this_opt_d_TW = sess.run(_d_TW)[0]
		this_opt_delta_l0 = sess.run(_delta_l0)[0]
		
		opt_parameters.append((niterations, error, this_opt_a_TOA, this_opt_c_TOA, this_opt_d_TOA, this_opt_a_TW, this_opt_c_TW, this_opt_d_TW, this_opt_delta_l0))
		niterations += 1
		if niterations>500:
			if np.median(error_cost[niterations-101:niterations-1]) > np.median(error_cost[niterations-201:niterations-101]):
				break

		if niterations>NMAXFOR2DMINIMISATION:
			break

	averaged_errors = []

	for niteration in range(0, len(opt_parameters)-51):
		if niteration<50:
			averaged_errors.append(0.)
		else:
			averaged_errors.append(np.mean([opt_parameters[niteration+shift][1] for shift in range(-50, 51)]))

	initial_error = np.array([error_cost[0]]*len(error_cost))
	averaged_errors = np.array(averaged_errors)
	min_error_index = np.argmin(averaged_errors[averaged_errors>0])+50
	if (error_cost[min_error_index] > error_cost[0]):
		min_error_index = 0

	precalib_a_TOA = opt_parameters[0][2]
	precalib_c_TOA = opt_parameters[0][3]
	precalib_d_TOA = opt_parameters[0][4]
	precalib_a_TW = opt_parameters[0][5]
	precalib_c_TW = opt_parameters[0][6]
	precalib_d_TW = opt_parameters[0][7]
	precalib_delta_l0 = opt_parameters[0][8]

	opt_a_TOA = opt_parameters[min_error_index][2]
	opt_c_TOA = opt_parameters[min_error_index][3]
	opt_d_TOA = opt_parameters[min_error_index][4]
	opt_a_TW = opt_parameters[min_error_index][5]
	opt_c_TW = opt_parameters[min_error_index][6]
	opt_d_TW = opt_parameters[min_error_index][7]
	opt_delta_l0 = opt_parameters[min_error_index][8]

	axs[0][4].plot(error_cost, "darkgreen", label="minimisation error")
	axs[0][4].plot(averaged_errors, "darkblue", label="averaged error")
	axs[0][4].plot(initial_error, "magenta", label="initial error")
	axs[0][4].set_yscale("log")
	axs[0][4].set_title("4) Unbinned 2D fit (Tensorflow)", fontsize=20)
	axs[0][4].set_xlabel('Iteration index')
	axs[0][4].set_ylabel('Cost [a.u.]')
	axs[0][4].axvline( x=opt_parameters[min_error_index][0], ymin=0., ymax=max(error_cost), color="red", label="Minimisation stop")
	axs[0][4].legend()

	print "Interrupted after %i iterations"%niterations
	print "Optimal error after %i iterations: %s" % (opt_parameters[min_error_index][0], opt_parameters[min_error_index][1])
	print "a_TOA:", opt_a_TOA
	print "c_TOA:", opt_c_TOA
	print "d_TOA:", opt_d_TOA
	print "a_TW:", opt_a_TW
	print "c_TW:", opt_c_TW
	print "d_TW:", opt_d_TW
	print "delta_l0:", opt_delta_l0


	#b) apply the calibration
	calib_hit_times = opt_delta_l0 + opt_a_TOA*selected_samples[TOA_TYPE] + opt_c_TOA/(selected_samples[TOA_TYPE]-opt_d_TOA) + (opt_a_TW*selected_samples["hit_energy"] + opt_c_TW/(selected_samples["hit_energy"]-opt_d_TW))
	#extend the data frame
	selected_samples = selected_samples.assign(calib_hit_time =  calib_hit_times)
	selected_samples = selected_samples.assign(delta_calib_hit_time =  calib_hit_times-selected_samples["TCorrected"])
	axs[1][4].hist2d(selected_samples["delta_calib_hit_time"], selected_samples["hit_energy"], bins=50, range=[[-2.5, 2.5],[0., 1000.]], cmap=plt.cm.Reds, label="Data, weighted", weights=selected_samples.weights_for_tf_min)
	axs[1][4].set_title('--> Timestamp difference vs. energy')
	axs[1][4].set_xlabel("RECO-TRUTH [ns]")
	axs[1][4].set_ylabel("hit energy [MIPs]")
	axs[1][4].legend()

	#c) visualise the difference to the MCP timestamp
	frequencies, bins, patches = axs[2][4].hist(selected_samples.delta_calib_hit_time, color="black", bins=100, range=(-5., 5.), align="mid", density=True, label="Data, weighted", weights=selected_samples.weights_for_tf_min)
	data_for_gaussian_fit = []
	for i in range(len(frequencies)):
		data_for_gaussian_fit.append((0.5*(bins[i]+bins[i+1]), frequencies[i]))
	(mu, sigma) = norm.fit(selected_samples[abs(selected_samples.delta_calib_hit_time)<5.].delta_calib_hit_time)
	y_gaus = norm.pdf( bins, mu, sigma)
	axs[2][4].plot(bins, y_gaus, 'gold', linewidth=2, label=r"gaus-fit: $\mu$=%5.3fns, $\sigma$=%5.3fns" % (mu, sigma))
	axs[2][4].set_title('--> Timestamp difference')
	axs[2][4].set_xlabel("RECO-TRUTH [ns]")
	axs[2][4].set_ylabel("Frequency (normalised)")
	axs[2][4].legend()

	
	calib_data.append((Module, Chip, Channel, {"rise": 1, "fall": -1}[toaType], opt_a_TOA, opt_c_TOA, opt_d_TOA, opt_a_TW, opt_c_TW, opt_d_TW, opt_delta_l0, precalib_a_TOA, precalib_c_TOA, precalib_d_TOA, precalib_a_TW, precalib_c_TW, precalib_d_TW, precalib_delta_l0))

except:
	calib_data.append((Module, Chip, Channel, {"rise": 1, "fall": -1}[toaType], -999, -999, -999, -999, -999, -999, -999, -999, -999, -999, -999, -999, -999, -999))
	print "ERROR-->Execution is interrupted"

fig.suptitle("Module %i, Chip %i, Channel %i, TOA-%s" % (Module, Chip, Channel, toaType), fontsize=30)
fig.savefig(args.outputFilePath.replace(".h5", ".png"))

calib_data = pd.DataFrame(calib_data, columns=["Module", "Chip", "Channel", "toa_type", "a_TOA", "c_TOA", "d_TOA", "a_TW", "c_TW", "d_TW", "delta_l0", "precalib_a_TOA", "precalib_c_TOA", "precalib_d_TOA", "precalib_a_TW", "precalib_c_TW", "precalib_d_TW", "precalib_delta_l0"])
outstore = pd.HDFStore(args.outputFilePath)
outstore["calib"] = calib_data
outstore.close()
