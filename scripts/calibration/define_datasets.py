#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Determines the channels for which the number of entries with energies 
#above a given threshold is sufficient for subsequent time calibration.
#Splits the full dataset into dataset used for calibration and one used for evaluation.
#Writes out a list of those channels both as a .h5 file and (optional) as an ntuple for visualisation (requires ROOT).

from config.analysis import CHANNELSELECTIONPARAMETERS

NMIN_ABOVE_EMIN_FOR_CALIB = CHANNELSELECTIONPARAMETERS["NMIN_ABOVE_EMIN_FOR_CALIB"]
EMIN_FORCALIBRATION = CHANNELSELECTIONPARAMETERS["EMIN_FORCALIBRATION"]
EVALRUNS = CHANNELSELECTIONPARAMETERS["EVALRUNS"]

import pandas as pd
import numpy as np
from tqdm import tqdm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFiles', nargs='+', help='Input files', required=True)
parser.add_argument("--outputFilePath", type=str, help="path to the output h5 that indicates which channels are to be calibrated", default="/home/tquast/tmp/merged_runs.h5", required=True)
parser.add_argument("--outputFileCalibration", type=str, help="path to the output h5 that contains the samples for calibration", default="/home/tquast/tmp/merged_runs.h5", required=True)
parser.add_argument("--outputFileEvaluation", type=str, help="path to the output h5 that contains the samples for evaluation", default="/home/tquast/tmp/merged_runs.h5", required=True)
parser.add_argument("--outputFilePathForVisualisation", type=str, help="Path for the output file that visualises the location of the investigated channels", default="", required=False)
parser.add_argument("--referenceNtuplePath", type=str, help="path to to ntuple to create channel map from, only useful if channels are to be visualised", default="", required=False)
args = parser.parse_args()



#reading channels in files for calibration
merged_minmax = None
merged_data_for_calib = None
for infilepath in args.inputFiles:
	print "Reading", infilepath
	store = pd.HDFStore(infilepath)
	if merged_minmax is None:
		merged_minmax = store["minmax"]
		merged_data_for_calib = store["minmaxcorrected"]
	else:
		merged_minmax = pd.concat([merged_minmax, store["minmax"]])
		merged_data_for_calib = pd.concat([merged_data_for_calib, store["minmaxcorrected"]])
	store.close()
print "Total number of entries:", len(merged_data_for_calib.channelkey)
all_channels_available = merged_minmax.channelkey.unique()


#splitting calibration and evaluation dataset
merged_data_for_eval = []
for eval_run in tqdm(EVALRUNS, unit="eval-runs"):
	merged_data_for_eval.append(merged_data_for_calib[merged_data_for_calib.run==eval_run].copy())
	merged_data_for_calib = merged_data_for_calib[merged_data_for_calib.run!=eval_run]

print "Writing evaluation file to", args.outputFileEvaluation
merged_data_for_eval = pd.concat(merged_data_for_eval)
outstore_evaluation = pd.HDFStore(args.outputFileEvaluation)
outstore_evaluation["evaluation"] = merged_data_for_eval
outstore_evaluation.close()

outstore_calibration = pd.HDFStore(args.outputFileCalibration)
channels_for_calibration =[]
for channel_candidate in tqdm(all_channels_available, unit="channel-candidate"):
	N_entries_for_calib = len(merged_data_for_calib.channelkey[(merged_data_for_calib.channelkey==channel_candidate)&(merged_data_for_calib.hit_energy>EMIN_FORCALIBRATION)])
	if N_entries_for_calib > NMIN_ABOVE_EMIN_FOR_CALIB:
		channels_for_calibration.append(channel_candidate)
		outstore_calibration["ch_%i"%channel_candidate] = merged_data_for_calib[merged_data_for_calib.channelkey==channel_candidate]
outstore_calibration.close()

with open(args.outputFilePath, "w") as channel_file:
	for chkey in channels_for_calibration:
		channel_file.write("%i \n"%chkey)



#optional: write list of channels to a file for visualisation with this event display program
#https://github.com/ThorbenQuast/HGCal_TB_Geant4
if args.outputFilePathForVisualisation != "":
	import os
	import ROOT
	import root_numpy as rn
	if not os.path.exists(args.outputFilePathForVisualisation):
		ch_to_pos_map = {}

		ntupleTreeFile = ROOT.TFile(args.referenceNtuplePath, "READ")
		ntupleTree = ntupleTreeFile.Get("rechitntupler/hits")
		ntupleData = rn.tree2array(ntupleTree, branches=["rechit_%s" % quantity for quantity in ["module", "chip", "channel", "x", "y", "z", "layer"]])

		for ev in tqdm(range(len(ntupleData["rechit_module"])), unit="ntuple entries"):
			for nhit in range(len(ntupleData["rechit_module"][ev])):
				key = ntupleData["rechit_module"][ev][nhit]*1000+ntupleData["rechit_chip"][ev][nhit]*100+ntupleData["rechit_channel"][ev][nhit]
				if key in ch_to_pos_map:
					continue
				ch_to_pos_map[key] = (ntupleData["rechit_x"][ev][nhit], ntupleData["rechit_y"][ev][nhit], ntupleData["rechit_z"][ev][nhit], ntupleData["rechit_layer"][ev][nhit])


		output_dtypes = []	
		output_dtypes.append(("calib_layer", np.int32))	
		output_dtypes.append(("calib_module", np.int32))	
		output_dtypes.append(("calib_chip", np.int32))	
		output_dtypes.append(("calib_channel", np.int32))	
		output_dtypes.append(("calib_MPV", np.float32))
		output_dtypes.append(("calib_x", np.float32))	
		output_dtypes.append(("calib_y", np.float32))	
		output_dtypes.append(("calib_z", np.float32))	
		table = []
		for key in channels_for_calibration:
			Module = int(key/1000)
			ASIC = int((key%1000)/100)
			Channel = int((key%100))
			
			MPV = 40
			if not key in ch_to_pos_map:
				print key,"not found in map for configuration", config
				continue
			x = ch_to_pos_map[key][0]
			y = ch_to_pos_map[key][1]
			z = ch_to_pos_map[key][2]
			layer = ch_to_pos_map[key][3]			
			table.append((layer, Module, ASIC, Channel, MPV, x, y, z))
		outtree = rn.array2tree(np.array(table, dtype=output_dtypes))
		print "saving to",args.outputFilePathForVisualisation
		outfile = ROOT.TFile(args.outputFilePathForVisualisation, "RECREATE")
		outtree.Write()
		outfile.Close()
