#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Evaluates the timing performance of single channels in terms of accuracy and precision as a function
#of the hit energy. Produces the relevant distributions and graphs as graphic files.
#Applied both for the TOA-rise/fall and both combined.

from config.analysis import CHANNELEVALUATIONPARAMETERS
MINHITENERGY = CHANNELEVALUATIONPARAMETERS["MINHITENERGY"]
MINENTRIESFOREVAL = CHANNELEVALUATIONPARAMETERS["MINENTRIESFOREVAL"]
MINAMPMCP1 = CHANNELEVALUATIONPARAMETERS["MINAMPMCP1"]

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
from scipy.stats import norm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=False)
parser.add_argument("--channelKey", type=int, help="path to to ntuple to create channel map from", default=32228, required=False)
parser.add_argument("--outputFilePath_Raw", type=str, help="File path for visualisation", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_raw.pdf", required=False)
parser.add_argument("--outputFilePath_Graph", type=str, help="File path for visualisation", default="/home/tquast/tbOctober2018_H2/timing_analysis/channel_evaluation/32227_graphs.pdf", required=False)
args = parser.parse_args()

Module, Chip, Channel = int(args.channelKey/1000), int((args.channelKey%1000)/100), int(args.channelKey%100)

#reading channels in files for calibration
store = pd.HDFStore(args.inputFileEvaluation)
merged_data_calib = store["evaluation_calibrated"]
store.close()

#prepare the figures
fig1, axs1 = plt.subplots(4, 5, figsize=(30,20))
fig2, axs2 = plt.subplots(1, 3, figsize=(18,5))

#step 0:
#select good timing data
selected_samples = merged_data_calib[(merged_data_calib.TCorrected>=0)&(merged_data_calib.TCorrected<=25.)&(merged_data_calib.hit_energy>MINHITENERGY)]
selected_samples = selected_samples[selected_samples.ampMCP1>MINAMPMCP1]

selected_samples = selected_samples[selected_samples.channelkey==args.channelKey]


colors = {
	"calibrated_hit_time_combined": "black",	
	"calibrated_hit_time_rise": "blue",	
	"calibrated_hit_time_fall": "red",	
}

if len(selected_samples.hit_energy) > MINENTRIESFOREVAL:
	x_energies = []
	y_mus = {}
	y_mus_error = {}
	y_sigmas = {}
	y_sigmas_error = {}
	for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
		y_mus[toa_type] = []
		y_mus_error[toa_type] = []
		y_sigmas[toa_type] = []
		y_sigmas_error[toa_type] = []



	for drawindex, interval in enumerate(sorted(pd.cut(selected_samples["hit_energy"], bins=20).unique())):
		selected_indexes = selected_samples.hit_energy.between(interval.left, interval.right)
		energy_entries = selected_samples.hit_energy[selected_indexes]
		if len(energy_entries)<100:
			continue
		Emean = np.mean(energy_entries)
		this_ax = axs1[drawindex/5][drawindex%5]
		x_energies.append(Emean)
		
			
		for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
			time_difference_ps = 1000.*(selected_samples[selected_indexes][toa_type]-selected_samples[selected_indexes].TDoubleCorrected)

			nbins = 60
			frequencies, bins, patches = this_ax.hist(time_difference_ps, bins=nbins, range=[-1500., 1500.], density=True, color=colors[toa_type])
			mu = np.median(time_difference_ps)
			iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
			sigma = 0.5*(iqr_84-iqr_16)

			y_gaus = norm.pdf( bins, mu, sigma)
			this_ax.plot(bins, y_gaus, colors[toa_type], linewidth=2, label=r"%s: $\mu$=%5.3fps, $\sigma$=%5.3fps" % (toa_type.replace("calibrated_", ""), mu, sigma))
			

			y_mus[toa_type].append(mu)
			y_mus_error[toa_type].append(0.)
			y_sigmas[toa_type].append(sigma)
			y_sigmas_error[toa_type].append(0.)

		this_ax.set_title("E = %.1f - %.1f MIPs" % (interval.left, interval.right), fontsize=10)
		this_ax.legend()
		this_ax.set_xlabel("Hit time - MCP time [ps]")

	fig1.suptitle("Module %i, Chip %i, Channel %i" % (Module, Chip, Channel), fontsize=30)


	#plot the graphs

	axs2[0].hist2d(selected_samples.calibrated_hit_time_rise, selected_samples.calibrated_hit_time_fall, bins=50, range=[[-5., 30.],[-5., 30.]], label="Data")
	axs2[0].set_xlabel("Calibrated toa-rise [ns]")
	axs2[0].set_ylabel("Calibrated toa-fall [ns]")
	for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
		axs2[1].errorbar(np.array(x_energies), np.array(y_mus[toa_type]), yerr=np.array(y_mus_error[toa_type]), label=toa_type.replace("calibrated_", ""), color= colors[toa_type])
	axs2[1].axhline(linewidth=1, color='gold')
	axs2[1].set_xlabel("Hit energy [MIPs]")
	axs2[1].set_ylabel("<Time difference> [ps]")
	axs2[1].legend()
	for toa_type in ["calibrated_hit_time_combined", "calibrated_hit_time_rise", "calibrated_hit_time_fall"]:
		axs2[2].errorbar(np.array(x_energies), np.array(y_sigmas[toa_type]), yerr=np.array(y_sigmas_error[toa_type]), label=toa_type.replace("calibrated_", ""), color= colors[toa_type])
	axs2[2].set_xlabel("Hit energy [MIPs]")
	axs2[2].set_ylabel("Width(Time difference) [ps]")
	axs2[2].legend()
	fig2.suptitle("Module %i, Chip %i, Channel %i" % (Module, Chip, Channel), fontsize=30)



fig1.savefig(args.outputFilePath_Raw)
fig2.savefig(args.outputFilePath_Graph)
