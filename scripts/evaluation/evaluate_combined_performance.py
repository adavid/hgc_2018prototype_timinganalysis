#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Evaluation strategy as for the channels but applied to the event timestamps from many channels combined.
#Applied both for the TOA-rise/fall and both combined.

from config.analysis import EVALUATIONPARAMETERS
MINAMPMCP1 = EVALUATIONPARAMETERS["MINAMPMCP1"]

import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import pandas as pd
import numpy as np
from scipy.stats import norm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFile', type=str, help='Input file for evaluation', default="/home/tquast/tbOctober2018_H2/timing_analysis/performance/energy_weighted_sums.h5", required=False)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums", default="/home/tquast/tbOctober2018_H2/timing_analysis/performance/performance.pdf", required=False)
args = parser.parse_args()

#reading channels in files for calibration
store = pd.HDFStore(args.inputFile)
processed_data = store["processed"]
store.close()

beamenergies = sorted(processed_data.beamenergy.unique())
fig1, axs1 = plt.subplots(3, 3, figsize=(18, 15))
fig2, axs2 = plt.subplots(1, 3, figsize=(18,5))


colors = {
	"deltaT_toa_combined": "black",	
	"deltaT_toa_rise": "blue",	
	"deltaT_toa_fall": "red",	
}

deltaT_types = ["deltaT_toa_rise", "deltaT_toa_fall", "deltaT_toa_combined"]

x_energies = []
y_mus = {}
y_mus_error = {}
y_sigmas = {}
y_sigmas_error = {}
for toa_type in deltaT_types:
	y_mus[toa_type] = []
	y_mus_error[toa_type] = []
	y_sigmas[toa_type] = []
	y_sigmas_error[toa_type] = []



for drawindex, beamenergy in enumerate(beamenergies):
	nbins=50
	beamenergy_selected_data = processed_data[(processed_data.beamenergy==beamenergy)]
	beamenergy_selected_data = beamenergy_selected_data[beamenergy_selected_data.ampMCP1>MINAMPMCP1]
	x_energies.append(beamenergy*1.)

	this_ax = axs1[drawindex/3][drawindex%3]

	for toa_type in deltaT_types:
		time_difference_ps = beamenergy_selected_data[toa_type]

		frequencies, bins, patches = this_ax.hist(time_difference_ps, bins=nbins, range=[-500., 500.], density=True, color=colors[toa_type], label="")
		mu = np.median(time_difference_ps)
		iqr_16, iqr_84 =  np.array(time_difference_ps.quantile([0.16, 0.84]))
		sigma = 0.5*(iqr_84-iqr_16)

		y_gaus = norm.pdf( bins, mu, sigma)
		this_ax.plot(bins, y_gaus, colors[toa_type], linewidth=2, label=r"%s: $\mu$=%5.3fps, $\sigma$=%5.3fps" % (toa_type.replace("deltaT_", ""), mu, sigma))
		
		y_mus[toa_type].append(mu)
		y_mus_error[toa_type].append(0.)
		y_sigmas[toa_type].append(sigma)
		y_sigmas_error[toa_type].append(0.)


	this_ax.set_xlabel("Time difference [ps]")
	this_ax.set_ylabel("Frequency (normalised)")
	this_ax.legend()
	this_ax.set_title("%i GeV $e^{+}$" % beamenergy, fontsize=14)


axs2[0].hist2d(processed_data.deltaT_toa_rise, processed_data.deltaT_toa_fall, bins=50, range=[[-500., 500.],[-500., 500.]], label="Data")
axs2[0].set_xlabel("Calibrated toa-rise time (energy-weighted channels) [ps]")
axs2[0].set_ylabel("Calibrated toa-fall time (energy-weighted channels) [ps]")
for toa_type in deltaT_types:
	axs2[1].errorbar(np.array(x_energies), np.array(y_mus[toa_type]), yerr=np.array(y_mus_error[toa_type]), label=toa_type.replace("deltaT_", ""), color= colors[toa_type])
axs2[1].axhline(linewidth=1, color='r')
axs2[1].set_xlabel("Positron energy [GeV]")
axs2[1].set_ylabel("<Combined time difference to MCP> [ps]")
axs2[1].legend()
for toa_type in deltaT_types:
	axs2[2].errorbar(np.array(x_energies), np.array(y_sigmas[toa_type]), yerr=np.array(y_sigmas_error[toa_type]), label=toa_type.replace("deltaT_", ""), color= colors[toa_type])
axs2[2].set_xlabel("Positron energy [GeV]")
axs2[2].set_ylabel("IQR68(Combined time difference to MCP) [ps]")
axs2[2].legend()
fig2.suptitle("$e^{+}$-timing performance", fontsize=30)


fig1.savefig(args.outputFilePath.replace(".png", "_distributions.png"))
fig2.savefig(args.outputFilePath)