#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Template to merge files, counts occurence of channels for min/max identification.
#Normalises the TOA and writes out the affected channels into a file.
#Only those channels in the set of runs can be used for subsequent calibration & timing performance assessment.
from config.analysis import MINMAXPARAMETERS
NMINFORANALYSIS = MINMAXPARAMETERS["NMINFORANALYSIS"]

import pandas as pd
from tqdm import tqdm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFiles', nargs='+', help='Input files', required=True)
parser.add_argument("--outputFilePath", type=str, help="path to the output h5 file", default="/home/tquast/tmp/merged_runs.h5", required=False)
args = parser.parse_args()


merged_data = None
for infilepath in tqdm(args.inputFiles, unit="input files"):
	store = pd.HDFStore(infilepath)
	if merged_data is None:
		merged_data = store["pd"]
	else:
		merged_data = pd.concat([merged_data, store["pd"]])
	store.close()
	#print "+",len(store["pd"]),"=",len(merged_data)	
merged_data = merged_data.assign(unique_event_index = 1e6*merged_data["run"]+merged_data["event"])
print "Determining unique channels in the merged file"
counts = merged_data.groupby(["channelkey"])["unique_event_index"].nunique()

min_max_array = []


minmax_corrected_data = []
for chkey, count in tqdm(counts.items(), unit="channels"):
	if count < NMINFORANALYSIS:
		continue

	df_channel = merged_data[merged_data.channelkey==chkey].copy()

	TOA_rise_min = df_channel.toa_rise.min()
	TOA_rise_max = df_channel.toa_rise.max()
	TOA_fall_min = df_channel.toa_fall.min()
	TOA_fall_max = df_channel.toa_fall.max()

	min_max_array.append((chkey, TOA_rise_min, TOA_rise_max, TOA_fall_min, TOA_fall_max))
	

	#normalise the TOAs
	df_channel.toa_rise = 1.*(df_channel.toa_rise-TOA_rise_min) / (TOA_rise_max-TOA_rise_min)
	df_channel.toa_fall = 1.*(df_channel.toa_fall-TOA_fall_min) / (TOA_fall_max-TOA_fall_min)


	minmax_corrected_data.append(df_channel)


print "Concatenating %i dataframes" % len(minmax_corrected_data)
minmax_corrected_data = pd.concat(minmax_corrected_data)

print "Saving min-max corrected entries to",args.outputFilePath
outstore = pd.HDFStore(args.outputFilePath)
#write out the selected channels
outstore["minmaxcorrected"] = minmax_corrected_data

print "Saving min-max to file"
pd_min_max = pd.DataFrame(min_max_array, columns=["channelkey", "TOA_rise_min", "TOA_rise_max", "TOA_fall_min", "TOA_fall_max"])
outstore["minmax"] = pd_min_max
outstore.close()