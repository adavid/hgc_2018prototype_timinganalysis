#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Reads the ntuple content and saves timing information as flat pandas data frames for subsequent analysis independent of ROOT.

from config.analysis import MINAMP_MCP1, SPEED_OF_LIGHT

from root_pandas import read_root
import pandas as pd
from tqdm import tqdm

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--inputFilePath", type=str, help="path to the input ntuple", default="/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16/ntuple_1005.root", required=False)
parser.add_argument("--outputFilePath", type=str, help="path to the output h5 file", default="/home/tquast/tmp/run_1005.h5", required=False)
args = parser.parse_args()

df_rechits = read_root(args.inputFilePath, "rechitntupler/hits", columns=["event", "run", "rechit_x", "rechit_y", "rechit_z", "rechit_module", "rechit_chip", "rechit_channel", "rechit_energy", "rechit_toaRise", "rechit_toaFall"])
df_dwc = read_root(args.inputFilePath, "trackimpactntupler/impactPoints", columns=["event", "run", "dwcReferenceType", "b_x", "b_y"])
df_mcp = read_root(args.inputFilePath, "MCPntupler/MCP", columns=["event", "run", "ampFit_MCP1", "ampFit_MCP2", "TS_toClock_FE_MCP1", "TS_toClock_FE_MCP2"])

NEvents = len(df_rechits)

flat_data = []

for nevent in tqdm(range(NEvents), unit="evt"):
	event = df_rechits.event[nevent]
	run = df_rechits.run[nevent]
	ampFit_MCP1 = df_mcp.ampFit_MCP1[nevent]
	TS_toClock_FE_MCP1 = df_mcp.TS_toClock_FE_MCP1[nevent]
	if ampFit_MCP1 < MINAMP_MCP1:
		continue

	dwcReferenceType = df_dwc.dwcReferenceType[nevent]
	if dwcReferenceType!=13:		#One should require good DWC track if we want to assess the TOF from incidence to the cell eventually.
		continue

	dwc_bx = df_dwc.b_x[nevent]
	dwc_by = df_dwc.b_y[nevent]

	NHit = len(df_rechits.rechit_module[nevent])
	rechits_module = df_rechits.rechit_module[nevent]
	rechits_chip = df_rechits.rechit_chip[nevent]
	rechits_channel = df_rechits.rechit_channel[nevent]
	rechits_x = df_rechits.rechit_x[nevent]
	rechits_y = df_rechits.rechit_y[nevent]
	rechits_z = df_rechits.rechit_z[nevent]
	rechits_energy = df_rechits.rechit_energy[nevent]
	rechits_toaRise = df_rechits.rechit_toaRise[nevent]
	rechits_toaFall = df_rechits.rechit_toaFall[nevent]

	for nhit in range(NHit):
		_toa_rise = rechits_toaRise[nhit]
		_toa_fall = rechits_toaFall[nhit]
		if _toa_rise==4:
			continue
		if _toa_fall==4:
			continue

		_x = rechits_x[nhit]
		_y = rechits_y[nhit]
		_z = rechits_z[nhit]
		_energy = rechits_energy[nhit]

		_module = rechits_module[nhit]
		_chip = rechits_chip[nhit]
		_channel = rechits_channel[nhit]

		ch_key = 1000*_module+100*_chip+_channel

		corrected_time = TS_toClock_FE_MCP1 + _z/SPEED_OF_LIGHT
		if corrected_time > 25.:
			corrected_time = corrected_time - 25.		#25ns periodicity
			
		flat_data.append((event, run, ch_key, _energy, _toa_rise, _toa_fall, ampFit_MCP1, TS_toClock_FE_MCP1, corrected_time, dwc_bx, dwc_by))


pd_output = pd.DataFrame(flat_data, columns=["event", "run", "channelkey", "hit_energy", "toa_rise", "toa_fall", "ampMCP1", "TMCP1", "TCorrected", "dwc_bx", "dwc_by"])
#store in external file
store = pd.HDFStore(args.outputFilePath)
store["pd"] = pd_output
store.close()