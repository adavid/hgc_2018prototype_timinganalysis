#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Applies the derived calibration constants to derive hit timestamps for TOA fall and rise independently.
#The TOA fall is corrected by T/2 = 12.5ns to match the TOA rise based timestamp.
#The period of the MCP time is similarly corrected.

import os
import pandas as pd
import numpy as np
from tqdm import tqdm
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibration_preparation/samples_for_evaluation.h5", required=True)
parser.add_argument('--calibFile', type=str, help='Input file with the calibration', default="/home/tquast/tbOctober2018_H2/timing_analysis/calib_file.h5", required=True)
parser.add_argument("--outputFilePath", type=str, help="Path to write out the calibrated evaluation sample", default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=True)
parser.add_argument("--dataset", type=str, help="evaluation or calibration dataset", default="evaluation", required=True)
args = parser.parse_args()

print "Loading the data"
input_file_calibration = pd.HDFStore(args.calibFile)
calib_data = input_file_calibration["calib"]
input_file_calibration.close()

input_file_evaluation = pd.HDFStore(args.inputFileEvaluation)
if args.dataset=="evaluation":
	evaluation_samples = input_file_evaluation["evaluation"]
else:
	evaluation_samples = []
	for key in tqdm(input_file_evaluation, unit="channels"):
		evaluation_samples.append(input_file_evaluation[key])
	evaluation_samples = pd.concat(evaluation_samples)
input_file_evaluation.close()

#extending the calibration data by channel key to facilitate the matching to the samples
calib_data = calib_data.assign(channelkey = 1000*calib_data.Module+100*calib_data.Chip+calib_data.Channel)
evaluation_samples = evaluation_samples.assign(calibrated_hit_time_fall = -999)
evaluation_samples = evaluation_samples.assign(calibrated_hit_time_rise = -999)
evaluation_samples = evaluation_samples.assign(calibrated_hit_time_combined = -999)



#applying the correction
print "Applying the calibration..."
for channel in tqdm(calib_data.channelkey.unique(), unit="Channels"):
	#read the calibration for each channel
	calib_data_channel = calib_data[calib_data.channelkey==channel]
	calib_data_channel_rise = calib_data_channel[calib_data_channel.toa_type==1]
	calib_data_channel_fall = calib_data_channel[calib_data_channel.toa_type==-1]
	
	a_TOA_rise = np.array(calib_data_channel_rise.a_TOA)[0]
	c_TOA_rise = np.array(calib_data_channel_rise.c_TOA)[0]
	d_TOA_rise = np.array(calib_data_channel_rise.d_TOA)[0]
	a_TW_rise = np.array(calib_data_channel_rise.a_TW)[0]
	c_TW_rise = np.array(calib_data_channel_rise.c_TW)[0]
	d_TW_rise = np.array(calib_data_channel_rise.d_TW)[0]
	delta_l0_rise = np.array(calib_data_channel_rise.delta_l0)[0]

	a_TOA_fall = np.array(calib_data_channel_fall.a_TOA)[0]
	c_TOA_fall = np.array(calib_data_channel_fall.c_TOA)[0]
	d_TOA_fall = np.array(calib_data_channel_fall.d_TOA)[0]
	a_TW_fall = np.array(calib_data_channel_fall.a_TW)[0]
	c_TW_fall = np.array(calib_data_channel_fall.c_TW)[0]
	d_TW_fall = np.array(calib_data_channel_fall.d_TW)[0]
	delta_l0_fall = np.array(calib_data_channel_fall.delta_l0)[0]

	#compute the calibrated hit timestamps
	channel_samples = evaluation_samples[evaluation_samples.channelkey==channel]
	calib_hit_times_rise = delta_l0_rise + a_TOA_rise*channel_samples["toa_rise"] + c_TOA_rise/(channel_samples["toa_rise"]-d_TOA_rise) + (a_TW_rise*channel_samples["hit_energy"] + c_TW_rise/(channel_samples["hit_energy"]-d_TW_rise))
	calib_hit_times_fall = delta_l0_fall + a_TOA_fall*channel_samples["toa_fall"] + c_TOA_fall/(channel_samples["toa_fall"]-d_TOA_fall) + (a_TW_fall*channel_samples["hit_energy"] + c_TW_fall/(channel_samples["hit_energy"]-d_TW_fall))

	#store them to the main dataframe
	evaluation_samples.loc[evaluation_samples.channelkey==channel, "calibrated_hit_time_rise"] = calib_hit_times_rise
	evaluation_samples.loc[evaluation_samples.channelkey==channel, "calibrated_hit_time_fall"] = calib_hit_times_fall - 12.5 *  np.sign(calib_hit_times_fall-calib_hit_times_rise)		#correct for the periodicity difference by 12.5 seconds
	evaluation_samples.loc[evaluation_samples.channelkey==channel, "calibrated_hit_time_combined"] = 0.5*(evaluation_samples.loc[evaluation_samples.channelkey==channel, "calibrated_hit_time_rise"]+evaluation_samples.loc[evaluation_samples.channelkey==channel, "calibrated_hit_time_fall"])


#applying the period-correction for MCP-corrected timestamp
evaluation_samples = evaluation_samples.assign(TDoubleCorrected=evaluation_samples.TCorrected)
evaluation_samples.loc[evaluation_samples.calibrated_hit_time_combined-evaluation_samples.TCorrected < -12, "TDoubleCorrected"] -= 25.
evaluation_samples.loc[evaluation_samples.calibrated_hit_time_combined-evaluation_samples.TCorrected > 12, "TDoubleCorrected"] += 25.

input_file_evaluation_calibrated = pd.HDFStore(args.outputFilePath)
input_file_evaluation_calibrated["evaluation_calibrated"] = evaluation_samples
input_file_evaluation_calibrated.close()
