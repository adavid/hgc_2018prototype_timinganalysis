#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#Computes the common timestamp (or rather the combined deviation) of the calibrated hit timestamps
#with respect to the MCP time.
#Applied both for the TOA-rise/fall and both combined.

from config.analysis import COMMONTSPARAMETERS
MINHITENERGY = COMMONTSPARAMETERS["MINHITENERGY"]

import pandas as pd
import numpy as np
from tqdm import tqdm

from config.tb_ntuples import getBeamEnergyFromRun, getPDGIDFromRun


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--inputFileEvaluation', type=str, help='Input file for evaluation', default="/home/tquast/tbOctober2018_H2/timing_analysis/calibrated/evaluation_calibrated.h5", required=True)
parser.add_argument("--outputFilePath", type=str, help="File path to store the sums", default="/home/tquast/tbOctober2018_H2/timing_analysis/performance/energy_weighted_sums.h5", required=True)
args = parser.parse_args()

#reading channels in files for calibration
store = pd.HDFStore(args.inputFileEvaluation)
merged_data_calib = store["evaluation_calibrated"]
store.close()

out_data = []
run_indexes = merged_data_calib.run.unique()
for run_index in tqdm(run_indexes, unit="runs"):
	run_data = merged_data_calib[merged_data_calib.run==run_index]

	if getPDGIDFromRun(run_index) != 11:
		import sys
		sys.exit("Not an electron run!")
	beam_energy = getBeamEnergyFromRun(run_index)

	event_indexes = sorted(run_data.event.unique())
	for event in tqdm(event_indexes, unit="events"):
		event_data = run_data[run_data.event==event]
		#select only entries which are calibrated ...
		selected_event_data = event_data[(event_data.calibrated_hit_time_rise!=-999.)]
		#...and which have enough energy
		selected_event_data = selected_event_data[(event_data.hit_energy>MINHITENERGY)]
		if len(selected_event_data)==0:
			continue

		#compute an energy-weighted sum
		common_time_stamp_toa_rise = 1000.*np.sum(selected_event_data.hit_energy*(selected_event_data.calibrated_hit_time_rise-selected_event_data.TDoubleCorrected))/np.sum(selected_event_data.hit_energy)	#conversion into ps
		common_time_stamp_toa_fall = 1000.*np.sum(selected_event_data.hit_energy*(selected_event_data.calibrated_hit_time_fall-selected_event_data.TDoubleCorrected))/np.sum(selected_event_data.hit_energy)	#conversion into ps
		common_time_stamp_toa_combined = 1000.*np.sum(selected_event_data.hit_energy*(selected_event_data.calibrated_hit_time_combined-selected_event_data.TDoubleCorrected))/np.sum(selected_event_data.hit_energy)	#conversion into ps
		energy_sum = np.sum(selected_event_data.hit_energy)

		out_data.append((beam_energy, run_index, event, np.array(selected_event_data.ampMCP1)[0], common_time_stamp_toa_rise, common_time_stamp_toa_fall, common_time_stamp_toa_combined, energy_sum))


pd_out = pd.DataFrame(out_data, columns=["beamenergy", "run", "event", "ampMCP1", "deltaT_toa_rise", "deltaT_toa_fall", "deltaT_toa_combined", "energysum"])
outstore = pd.HDFStore(args.outputFilePath)
outstore["processed"] = pd_out
outstore.close()
