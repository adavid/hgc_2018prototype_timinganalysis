#Created: 25 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch

from config.analysis import MCPSTUDYPARAMETERS, OUTPUTDIRECTORY
MAXDELTAAMP = MCPSTUDYPARAMETERS["MAXDELTAAMP"]
MAXAMPMCP1 = MCPSTUDYPARAMETERS["MAXAMPMCP1"]
NBINSMCP = MCPSTUDYPARAMETERS["NBINSMCP"]

from root_pandas import read_root
import pandas as pd
import numpy as np
from tqdm import tqdm
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
from scipy.optimize import curve_fit
from math import sqrt

from config.tb_ntuples import *


merged_data = []
for run in tqdm(getRuns(configuration=23), unit="runs"):
	inputFilePath = get_ntuple_path(run)
	df_mcp = read_root(inputFilePath, "MCPntupler/MCP")
	merged_data.append(df_mcp)

merged_data = pd.concat(merged_data)
merged_data = merged_data[merged_data.valid_TS_MCP1*merged_data.valid_TS_MCP2==1]
merged_data = merged_data[merged_data.amp_MCP1<MAXAMPMCP1]
merged_data = merged_data[abs(merged_data.amp_MCP2-merged_data.amp_MCP1)<MAXDELTAAMP]

merged_data = merged_data.assign(delta_TS_MCP = 1000*(merged_data.TScf_MCP1-merged_data.TScf_MCP2))
merged_data = merged_data[abs(merged_data.delta_TS_MCP)<10000]
merged_data = merged_data.groupby(pd.cut(merged_data.amp_MCP1, NBINSMCP))


x_mean = np.array(merged_data.amp_MCP1.mean())
y_IQR68 = np.array(0.5*(merged_data.delta_TS_MCP.quantile(0.84)-merged_data.delta_TS_MCP.quantile(0.16)))/sqrt(2.)
y_std = np.array(merged_data.delta_TS_MCP.std())/sqrt(2.)
y_median = np.array(merged_data.delta_TS_MCP.median())
y_mean = np.array(merged_data.delta_TS_MCP.mean())

#reject NANs
y_IQR68 = y_IQR68[~np.isnan(x_mean)]
y_std = y_std[~np.isnan(x_mean)]
y_median = y_median[~np.isnan(x_mean)]
y_mean = y_mean[~np.isnan(x_mean)]
x_mean = x_mean[~np.isnan(x_mean)]

fig, axs = plt.subplots(1, 2, figsize=(12, 5))


#1. MCP Resolution estimates
axs[0].plot(x_mean, y_IQR68, color="black", label="68% IQR / 2", markersize=5, marker="o", linestyle=":")
axs[0].plot(x_mean, y_std, color="blue", label="RMS", markersize=5, marker="o", linestyle=":")

def func_resolution(x, c, n):
	return np.sqrt(n**2 / x**2 + c**2)

popt, pcov = curve_fit(func_resolution, x_mean, y_IQR68, maxfev=1000000, p0=(9.3, 15.2*1000)) 
x_points_for_plotting = np.linspace(min(x_mean), max(x_mean), 1000)
axs[0].plot(x_points_for_plotting, func_resolution(x_points_for_plotting, *popt), 'firebrick', label='fit: c=%5.3fps, n=%5.3fns*ADC' % (popt[0], popt[1]/1000))
axs[0].legend()
axs[0].set_title('Resolution estimate ($\Delta A$ < 50 ADC)', fontsize=20)
axs[0].set_xlabel("Amplitude MCP 1 [ADC]")
axs[0].set_ylabel("Resolution = Width / $\sqrt{2}$")

axs[1].plot(x_mean, y_median, color="green", label="Median")
axs[1].plot(x_mean, y_mean, color="magenta", label="Mean")
axs[1].legend()
axs[1].set_title('TOF ($\Delta A$ < 50 ADC)', fontsize=20)
axs[1].set_xlabel("Amplitude MCP 1 [ADC]")
axs[1].set_ylabel("<Time difference> between MCP2 and MCP2 [ps]")

fig.savefig(os.path.join(OUTPUTDIRECTORY, "mcp_time_resolution.png"))