import luigi
import os
from subprocess import Popen

from helpers.source import source
from config.analysis import OUTPUTDIRECTORY
from config.tb_ntuples import *


class TreatMinMax(luigi.Task):
    task_namespace = 'Timing'
    runNumberMin = luigi.IntParameter(default=1000, description="minimum run number", significant=True)
    runNumberMax = luigi.IntParameter(default=1100, description="maximum run number", significant=True)

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/preprocess/minmax_treatment.py")
        cmd += "--inputFiles "
        for inp in self.input():
            cmd += "%s " % inp.path
        cmd += "--outputFilePath %s " % self.output().path
        
        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")
        

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/minmax_corrected" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/toa_normalised_runs_%i_to_%i.h5" % (outputdir, self.runNumberMin, self.runNumberMax))

    def requires(self):
        inputs = []
        for run in getRuns(configuration=23):
            if not hasMCP(run):
                continue
            if not hasDWC(run):
                continue
            if run > self.runNumberMax:
                continue
            if run < self.runNumberMin:
                continue
            inputs.append(FlattenNtuple(runNumber=run))
        return inputs



class FlattenNtuple(luigi.Task):
    task_namespace = 'Timing'
    runNumber = luigi.IntParameter(default=0, description="run number", significant=True)

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/preprocess/reduce_ntuple.py")
        cmd += "--inputFilePath %s " % get_ntuple_path(self.runNumber)
        cmd += "--outputFilePath %s " % self.output().path
        
        print cmd
        env = {}
        env.update(os.environ)
        env.update(source("/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.34/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh"))
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")
        

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/flat_ntuples" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/reduced_ntuple_run%i.h5" % (outputdir, self.runNumber))

    def requires(self):
        pass