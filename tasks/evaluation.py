import luigi
import os
from subprocess import Popen

from config.analysis import OUTPUTDIRECTORY


class EvaluateCombinedTimingPerformance(luigi.Task):
    task_namespace = 'Timing'

    def output(self):
        outputdir = OUTPUTDIRECTORY+"/combined_performance"
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)        
        return luigi.LocalTarget("%s/performance.png" % outputdir)

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/evaluation/evaluate_combined_performance.py")
        cmd += "--inputFile %s " % self.input()["common"].path
        cmd += "--outputFilePath %s " % self.output().path    

        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")


    def requires(self):
    	from tasks.reco import ComputeCommonTimestamp
        return {
            "channel-wise": EvaluateTimingForAllChannels(),
            "common": ComputeCommonTimestamp()
        } 


class EvaluateTimingForAllChannels(luigi.Task):
    task_namespace = 'Timing'

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/channel_evaluation" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/dummy.txt" % outputdir)

    def run(self):
        dependencies = []
        import numpy as np
        with open(self.input()["list"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                dependencies.append(EvaluateChannelTimingPerformance(channelKey=channel[0]))
        yield dependencies

        with open(self.output().path, "w") as outfile:
            outfile.write("Done")

    def requires(self):
    	from tasks.calibration import DefineDatasets
        return DefineDatasets()



class EvaluateChannelTimingPerformance(luigi.Task):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/channel_evaluation" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return {
            "raw": luigi.LocalTarget("%s/%i_raw.pdf" % (outputdir, self.channelKey)),
            "graph": luigi.LocalTarget("%s/%i_graph.pdf" % (outputdir, self.channelKey))
        }

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/evaluation/evaluate_channel_performance.py")
        cmd += "--inputFileEvaluation %s " % self.input().path
        cmd += "--channelKey %i " % self.channelKey    
        cmd += "--outputFilePath_Raw %s " % self.output()["raw"].path    
        cmd += "--outputFilePath_Graph %s " % self.output()["graph"].path    

        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")

    def requires(self):
    	from tasks.reco import ApplyTimingCalibration
        return ApplyTimingCalibration(dataset="calibration")