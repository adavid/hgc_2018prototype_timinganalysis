import luigi
import os
from subprocess import Popen

from config.analysis import OUTPUTDIRECTORY


class ComputeCommonTimestamp(luigi.Task):
    task_namespace = 'Timing'

    def output(self):
        outputdir = OUTPUTDIRECTORY+"/combined_performance"
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)        
        return luigi.LocalTarget("%s/energy_weighted_sums.h5" % outputdir)

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/reco/compute_common_ts.py")
        cmd += "--inputFileEvaluation %s " % self.input().path
        cmd += "--outputFilePath %s " % self.output().path    

        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")


    def requires(self):
        return ApplyTimingCalibration(dataset="evaluation")



class ApplyTimingCalibration(luigi.Task):
    task_namespace = 'Timing'
    dataset = luigi.Parameter(default="evaluation", description="evaluation or training", significant=True)

    def output(self):
        outputdir = OUTPUTDIRECTORY+"/calibrated_samples"
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)        
        return luigi.LocalTarget("%s/%s_calibrated.h5" % (outputdir, self.dataset))

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/reco/apply_calibration.py")
        cmd += "--inputFileEvaluation %s " % self.input()["samples"][self.dataset].path
        cmd += "--calibFile %s " % self.input()["calib"].path
        cmd += "--outputFilePath %s " % self.output().path    
        cmd += "--dataset %s " % self.dataset

        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")


    def requires(self):
    	from tasks.calibration import DefineDatasets, MergeCalibrationFiles
        return {
            "calib": MergeCalibrationFiles(),
            "samples": DefineDatasets()
        }