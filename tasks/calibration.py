import luigi
import os
from subprocess import Popen
import numpy as np

from config.analysis import OUTPUTDIRECTORY, MINMAXPARAMETERS


class MergeCalibrationFiles(luigi.Task):
    task_namespace = 'Timing'

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/calibration" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/calib_file.h5" % outputdir)

    def run(self):
        calib_data = []
        
        import pandas as pd
        with open(self.input()["channel_list"]["list"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                for toaType in ["rise", "fall"]:
                    calibstore = pd.HDFStore(CalibrateTimingForChannel(channelKey=channel[0], toaType=toaType).output().path)
                    store_key = "calib"
                    calib_data.append(calibstore[store_key].copy())
        calib_data = pd.concat(calib_data)   
        full_calib_store =  pd.HDFStore(self.output().path)
        full_calib_store["calib"] = calib_data
        full_calib_store.close()

    def requires(self):
        return {
            "channel_list": DefineDatasets(),
            "calibration_procedure": CalibrateTimingForAllChannels()
        }


class CalibrateTimingForAllChannels(luigi.Task):
    task_namespace = 'Timing'

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/calibration" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/calib_summary.txt" % outputdir)

    def run(self):
        dependencies = []
        with open(self.input()["list"].path, "r") as infile:
            channels = np.genfromtxt(infile, dtype=[("channel", "i4")])
            for channel in channels:
                for toaType in ["rise", "fall"]:
                    dependencies.append(CalibrateTimingForChannel(channelKey=channel[0], toaType=toaType))
        yield dependencies
        

        with open(self.output().path, "w") as dummy_file:
            for dep in dependencies:
                dummy_file.write("Created %s \n"%dep.output().path)


    def requires(self):
        return DefineDatasets()


class CalibrateTimingForChannel(luigi.Task):
    task_namespace = 'Timing'
    channelKey = luigi.IntParameter(default=0, description="channel key to calibrate", significant=True)
    toaType = luigi.Parameter(default="fall", description="toa type (fall or rise)", significant=True)

    def output(self):
        outputdir = OUTPUTDIRECTORY + "/calibration" 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        return luigi.LocalTarget("%s/channel_%i_%s.h5" % (outputdir, self.channelKey, self.toaType))

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/calibration/calibrate_channel.py")
        cmd += "--inputFileCalib %s " % self.input()["calibration"].path
        cmd += "--channelKey %i " % self.channelKey    
        cmd += "--outputFilePath %s " % self.output().path    
        cmd += "--toaType %s " % self.toaType    

        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")

    def requires(self):
        return DefineDatasets()



class DefineDatasets(luigi.Task):
    task_namespace = 'Timing'

    def run(self):
        cmd = "python %s " % os.path.abspath("../scripts/calibration/define_datasets.py")
        cmd += "--inputFiles "
        for inp in self.input():
            cmd += "%s " % inp.path            
        cmd += "--outputFileCalibration %s " % self.output()["calibration"].path
        cmd += "--outputFileEvaluation %s " % self.output()["evaluation"].path
        cmd += "--outputFilePath %s " % self.output()["list"].path
        
        print cmd
        env = {}
        env.update(os.environ)
        p = Popen(cmd, stdout=None, stderr=None, shell=True, env=env)        
        out, err = p.communicate()
        code = p.returncode
        if code != 0:
            raise Exception("python analysis execution failed")


    def output(self):
        outputdir = OUTPUTDIRECTORY + "/datasets"
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)        
        return {
            "list": luigi.LocalTarget("%s/channel_list_for_timing_study.txt" % (outputdir)),
            "calibration": luigi.LocalTarget("%s/samples_for_calibration.h5" % (outputdir)),
            "evaluation": luigi.LocalTarget("%s/samples_for_evaluation.h5" % (outputdir))
        }


    def requires(self):
        from tasks.preprocess import TreatMinMax
        return [TreatMinMax(runNumberMin=r_min, runNumberMax=r_max) for r_min, r_max in MINMAXPARAMETERS["MINMAXCONSTRANGES"]]
       