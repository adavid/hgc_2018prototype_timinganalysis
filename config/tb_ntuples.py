#Created: 21 Feb 2020
#author: Thorben Quast, thorben.quast@cern.ch
#documentation of + utility related to accessing the input ntuples from the October 2018 beam test

import os

#path to the beam test ntuples
ntuple_directory = "/eos/cms/store/group/dpg_hgcal/tb_hgcal/2018/cern_h2_october/offline_analysis/ntuples/v16"
def get_ntuple_path(run):
	return os.path.join(ntuple_directory, "ntuple_%i.root"%run)

#run list of configuration 2 (==23) https://docs.google.com/spreadsheets/d/1FlwthMixJHmHVBguYSbw5mIeBo2tF7rFJPTYLTNI9Vs/edit#gid=290023431
TB_run_list = []
#Format: Run, PDG ID, beam energy, setup ID, valid DWC, valid XCET, valid MCP
TB_run_list.append((912, 300, 211, 23, True, True, True))
TB_run_list.append((916, 100, 11, 23, True, True, True))
TB_run_list.append((918, 300, 11, 23, True, True, True))
TB_run_list.append((919, 300, 11, 23, True, True, True))
TB_run_list.append((920, 300, 11, 23, True, True, True))
TB_run_list.append((921, 50, 11, 23, True, True, True))
TB_run_list.append((922, 50, 11, 23, True, True, True))
TB_run_list.append((923, 50, 11, 23, True, True, True))
TB_run_list.append((924, 50, 11, 23, True, True, True))
TB_run_list.append((925, 150, 11, 23, True, True, True))
TB_run_list.append((926, 150, 11, 23, True, True, True))
TB_run_list.append((927, 150, 11, 23, True, True, True))
TB_run_list.append((930, 150, 11, 23, True, True, True))
TB_run_list.append((932, 150, 11, 23, True, True, True))
TB_run_list.append((933, 150, 11, 23, True, True, True))
TB_run_list.append((934, 300, 211, 23, True, True, True))
TB_run_list.append((935, 300, 211, 23, True, True, True))
TB_run_list.append((936, 300, 211, 23, True, True, True))
TB_run_list.append((939, 300, 211, 23, True, True, True))
TB_run_list.append((940, 300, 211, 23, True, True, True))
TB_run_list.append((941, 50, 211, 23, True, True, True))
TB_run_list.append((942, 50, 211, 23, True, True, True))
TB_run_list.append((943, 50, 211, 23, True, True, True))
TB_run_list.append((944, 50, 211, 23, True, True, True))
TB_run_list.append((945, 50, 211, 23, True, True, True))
TB_run_list.append((946, 150, 211, 23, True, True, True))
TB_run_list.append((947, 150, 211, 23, True, True, True))
TB_run_list.append((948, 150, 211, 23, True, True, True))
TB_run_list.append((949, 150, 211, 23, True, True, True))
TB_run_list.append((950, 150, 211, 23, True, True, True))
TB_run_list.append((952, 150, 211, 23, True, True, True))
TB_run_list.append((953, 150, 211, 23, True, True, True))
TB_run_list.append((955, 150, 211, 23, True, True, True))
TB_run_list.append((956, 250, 211, 23, True, True, True))
TB_run_list.append((957, 250, 211, 23, True, True, True))
TB_run_list.append((958, 250, 211, 23, True, True, True))
TB_run_list.append((960, 50, 11, 23, True, True, True))
TB_run_list.append((961, 50, 11, 23, True, True, True))
TB_run_list.append((963, 50, 11, 23, True, True, True))
TB_run_list.append((964, 300, 11, 23, True, True, True))
TB_run_list.append((965, 300, 11, 23, True, True, True))
TB_run_list.append((966, 300, 11, 23, True, True, True))
TB_run_list.append((967, 300, 11, 23, True, True, True))
TB_run_list.append((968, 300, 11, 23, True, True, True))
TB_run_list.append((969, 300, 11, 23, True, True, True))
TB_run_list.append((970, 80, 11, 23, True, True, True))
TB_run_list.append((971, 80, 11, 23, True, True, True))
TB_run_list.append((972, 80, 11, 23, True, True, True))
TB_run_list.append((973, 80, 11, 23, True, True, True))
TB_run_list.append((974, 80, 11, 23, True, True, True))
TB_run_list.append((975, 80, 11, 23, True, True, True))
TB_run_list.append((976, 250, 11, 23, True, True, True))
TB_run_list.append((977, 250, 11, 23, True, True, True))
TB_run_list.append((978, 250, 11, 23, True, True, True))
TB_run_list.append((979, 250, 11, 23, True, True, True))
TB_run_list.append((980, 250, 11, 23, True, True, True))
TB_run_list.append((981, 250, 11, 23, True, True, True))
TB_run_list.append((984, 20, 11, 23, True, True, True))
TB_run_list.append((985, 100, 11, 23, True, True, True))
TB_run_list.append((987, 100, 11, 23, True, True, True))
TB_run_list.append((988, 100, 11, 23, True, True, True))
TB_run_list.append((990, 100, 11, 23, True, True, True))
TB_run_list.append((991, 100, 11, 23, True, True, True))
TB_run_list.append((992, 200, 11, 23, True, True, True))
TB_run_list.append((993, 200, 11, 23, True, True, True))
TB_run_list.append((994, 200, 11, 23, True, True, True))
TB_run_list.append((996, 200, 11, 23, True, True, True))
TB_run_list.append((997, 200, 11, 23, True, True, True))
TB_run_list.append((998, 120, 11, 23, True, True, True))
TB_run_list.append((999, 120, 11, 23, True, True, True))
TB_run_list.append((1000, 120, 11, 23, True, True, True))
TB_run_list.append((1001, 120, 11, 23, True, True, True))
TB_run_list.append((1002, 120, 11, 23, True, True, True))
TB_run_list.append((1005, 300, 11, 23, True, True, True))
TB_run_list.append((1006, 300, 11, 23, True, True, True))
TB_run_list.append((1007, 30, 11, 23, True, True, True))
TB_run_list.append((1008, 30, 11, 23, True, True, True))
TB_run_list.append((1010, 30, 11, 23, True, True, True))
TB_run_list.append((1011, 30, 11, 23, True, True, True))
TB_run_list.append((1012, 20, 11, 23, True, True, True))
TB_run_list.append((1014, 20, 11, 23, True, True, True))
TB_run_list.append((1015, 20, 11, 23, True, True, True))
TB_run_list.append((1016, 20, 211, 23, True, True, True))
TB_run_list.append((1017, 20, 211, 23, True, True, True))
TB_run_list.append((1018, 30, 211, 23, True, True, True))
TB_run_list.append((1021, 30, 211, 23, True, True, True))
TB_run_list.append((1023, 30, 211, 23, True, True, True))
TB_run_list.append((1027, 200, 211, 23, True, True, True))
TB_run_list.append((1029, 200, 211, 23, True, True, True))
TB_run_list.append((1031, 200, 211, 23, True, True, True))
TB_run_list.append((1033, 200, 211, 23, True, True, True))
TB_run_list.append((1036, 200, 211, 23, True, True, True))
TB_run_list.append((1037, 120, 211, 23, True, True, True))
TB_run_list.append((1038, 120, 211, 23, True, True, True))
TB_run_list.append((1040, 120, 211, 23, True, True, True))
TB_run_list.append((1042, 120, 211, 23, True, True, True))
TB_run_list.append((1043, 120, 211, 23, True, True, True))
TB_run_list.append((1045, 100, 211, 23, True, True, True))
TB_run_list.append((1047, 100, 211, 23, True, True, True))
TB_run_list.append((1049, 100, 211, 23, True, True, True))
TB_run_list.append((1051, 80, 211, 23, True, True, True))
TB_run_list.append((1052, 80, 211, 23, True, True, True))
TB_run_list.append((1053, 80, 211, 23, True, True, True))
TB_run_list.append((1054, 250, 211, 23, True, True, True))
TB_run_list.append((1055, 250, 211, 23, True, True, True))
TB_run_list.append((1056, 250, 211, 23, True, True, True))




	

#SUMMARY OF RUNS
def getRuns(pdgID=None, energy=None, configuration=None):
	runs = []
	for data in TB_run_list:
		_run = data[0]
		_pdgID = getPDGIDFromRun(_run)
		_energy = getBeamEnergyFromRun(_run)
		_config = getSetupIDFromRun(_run)
		if _pdgID==0:
			continue
		if pdgID!=None and pdgID!=_pdgID:
			continue
		if energy!=None and energy!=_energy:
			continue
		if configuration!=None and configuration!=_config:
			continue
		runs.append(_run)
	return runs

#utility functions
def scanRunlist(run, index):
	for data in TB_run_list:
		if data[0]==run:
			return data[index]
	return -1

def getBeamEnergyFromRun(run):
	return scanRunlist(run, 1)


def getPDGIDFromRun(run):
	return scanRunlist(run, 2)

def getSetupIDFromRun(run):
	return scanRunlist(run, 3)

def hasDWC(run):
	return scanRunlist(run, 4)

def hasXCET(run):
	return scanRunlist(run, 5)

def hasMCP(run):
	return scanRunlist(run, 6)	
