#main output directory to which all the files are to be written
OUTPUTDIRECTORY = "/home/tquast/tbOctober2018_H2/timing_analysis"

#speed of light for TOF computation, potentially useful (tbc)
SPEED_OF_LIGHT = 29.9792458	#cm/ns
#global event selection: minimum amplitude of MCP1
MINAMP_MCP1 = 100


#parameters for the estimation of the MCP-only timing resolution:
MCPSTUDYPARAMETERS = {
	#maximum amplitude difference between MCP 1 and MCP 2 amplitudes for estimating the combined resolution
	"MAXDELTAAMP": 50,
	#maximum amplitude of MCP1 assumed in this study
	"MAXAMPMCP1": 2000,
	#number of bins in the amplitude spectrum in which the combined resolution is to be estimated
	"NBINSMCP": 20
}

#parameters relevant for the determination of the TOA min and max
MINMAXPARAMETERS = {
	#must have at least 10k entries to have less than 1% probability that minimum or maximum value are not hit
	#n.b. p(either min or max not hit) = 2(1-1/x)^N-(1-2/x)^N, x=TOA range, N=number of entries in sample
	"NMINFORANALYSIS": 10000,
	#Run ranges in which the TOA pedestals are assumed to be constant <--> in which they are computed separately.
	"MINMAXCONSTRANGES": [(912, 958), (960, 1002), (1003, 1056)]
}

#parameters relevant for the selection of channels subject for calibration
CHANNELSELECTIONPARAMETERS = {
	#minimum energy for precalibration of the TOA, unit: MIPs
	"EMIN_FORCALIBRATION": 200,	
	#minimum number of entries with hit energies above EMIN_FORCALIBRATION such that channel is subject for calibration
	#if number of entries is smaller, the particular channel is not calibrated
	"NMIN_ABOVE_EMIN_FOR_CALIB": 20,
	#list of runs for ultimate performance assessment - not used in the calibration
	"EVALRUNS": [1012, 1007, 960, 971, 985, 998, 978, 967, 992]
}

#parameters relevant for the calibration of the TOA to the MCP timestamp
CALIBRATIONPARAMETERS = {
	#minimum hit energy for entries to be considered in the calibration, unit: MIPs
	"MINHITENERGY": 50,
	#number of TOA bins in the timewalk precalibration
	"NBINS_TW_PRECALIB": 20,
	#quantile definition for hit energy intervals for the timewalk precalibration
	"QUANTILERANGESFORTW": [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.93, 0.96, 0.98, 0.99, 1.],	
	#number of TOA bins in the TOA nonlinearity precalibration
	"NBINS_TOA_PRECALIB": 100,
	#minimum energy for entries to be included in the TOA precalibration, unit: MIPs
	"MINENERGYFORTWCALIB": 50,	
	#minimum energy for entries to be included in the TOA precalibration, unit: MIPs
	"MINENERGYFORTOACALIB": CHANNELSELECTIONPARAMETERS["EMIN_FORCALIBRATION"], 
	#maximum number of entries for the 2D unbinned fit with tensorflow to optimise the runtime
	"NMAXFOR2DMINIMISATION": 50000
}

#parameters relevant for the channel-wise evaluation of the timing performance
CHANNELEVALUATIONPARAMETERS = {
	#minimum hit energy for entries to be considered in the evaluation of the channel-wise timing performance
	"MINHITENERGY": 50,
	#minimum MCP 1 amplitude (higher --> less contribution to measured resolution from MCP1)
	"MINAMPMCP1": 500,
	#minimum number of entries for channel to be evaluated
	"MINENTRIESFOREVAL": 200
}

#parameters relevant to the computation of a common timestamp for each event
COMMONTSPARAMETERS = {
	#minimum hit energy for entries to be considered in the computation of an event timestamp, unit: MIPs
	"MINHITENERGY": 50
}

#parameters relevant for the combined evaluation of the timing performance
EVALUATIONPARAMETERS = {
	#minimum MCP 1 amplitude (higher --> less contribution to measured resolution from MCP1)
	"MINAMPMCP1": 500	
}